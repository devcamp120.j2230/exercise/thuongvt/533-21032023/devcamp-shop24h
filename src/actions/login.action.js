import auth from "../component/firebase/config";
import { signInWithPopup, GoogleAuthProvider, signOut } from "firebase/auth";
import { LOGIN_EMAIL_ERROR, LOGIN_EMAIL_SUCCESS, LOGIN_FETCH_ERROR, LOGIN_FETCH_PENDING, LOGIN_FETCH_SUCCESS, LOGOUT_FETCH_SUCCESS } from "../constants/login.constans";




const provider = new GoogleAuthProvider();

const fetchAPI = async (url, requestOptions) => {
    let res = await fetch(url, requestOptions);
    let data = await res.json()
    return data
}

export const login = () => {
    return async (dispatch) => {
        await dispatch({
            type: LOGIN_FETCH_PENDING
        })
        signInWithPopup(auth, provider)
            .then((result) => {
                // console.log(result.user)
                return dispatch({
                    type: LOGIN_FETCH_SUCCESS,
                    payload: result.user
                })
            })
            .catch((error) => {
                return dispatch({
                    type: LOGIN_FETCH_ERROR,
                    error: error
                })
            })
    }

}

export const logOut = () => {
    return async (dispatch) => {
        await dispatch({
            type: LOGIN_FETCH_PENDING
        })
        signOut(auth)
            .then(() => {
                // console.log(result)
                return dispatch({
                    type: LOGOUT_FETCH_SUCCESS,
                })
            })
            .catch((error) => {
                return dispatch({
                    type: LOGIN_FETCH_ERROR,
                    error: error
                })
            })
    }
}

export const logInWithEmail = (params) => {
    return async (dispatch) => {
        await dispatch({
            type: LOGIN_FETCH_PENDING
        })
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        fetchAPI("http://localhost:8000/login", { method: 'POST', headers: myHeaders, body: JSON.stringify(params), redirect: 'follow' })
            .then((res) => {
                if (res.message === "Tìm dữ liệu customer thành công") {
                    alert(`Đăng nhập thành công bằng email ${res.Data.email}`)
                    console.log(res.Data)
                    // Lưu Id của đối tượng người dùng, được dùng khi load lại trang
                    localStorage.setItem("Id",`${res.Data._id}`)
                    return dispatch ({
                        type: LOGIN_EMAIL_SUCCESS,
                        payload: res.Data,
                        token:res.accessToken,
                        
                    })
                }
                else {
                    alert("Bạn đã nhập sai mật khẩu hoặc password")
                }
            })
            .catch((err) => {
                console.error(err)
                alert(err.message)
                return dispatch ({
                    type: LOGIN_EMAIL_ERROR,
                    error:err
                })
                
            });
    }
}

export const LoginOutWithEmail =(e)=>{
    console.log(e)
    return async (dispatch) => {
        await dispatch({
            type: LOGIN_FETCH_PENDING
        })

        var myHeaders = new Headers();
        myHeaders.append(`token`,`bearer ${e}`);

        fetchAPI("http://localhost:8000/logOut", { method: 'POST', headers: myHeaders, redirect: 'follow' })
            .then((res) => {
                console.log(res)
                if (res.message === "LogOut thành công") {
                    return dispatch ({
                        type: LOGIN_EMAIL_SUCCESS,
                    })
                }
                else {
                    alert("Bạn đã chưa logUot")
                }
            })
            .catch((err) => {
                return dispatch ({
                    type: LOGIN_EMAIL_ERROR,
                    error:err
                })
                
            });
    }
}