import { MODAL_OPEN_CHECK, MODAL_OPEN_lOGIN } from "../constants/modal.constans"

export const ModalLogInOpenAction = ()=>{
    return {
        type: MODAL_OPEN_lOGIN,
        status: true
    }
}
export const ModalLogInCloseAction = () =>{
    return {
        type: MODAL_OPEN_lOGIN,
        status: false
    }
}
export const ModalCheckOpenAction = ()=>{
    return {
        type: MODAL_OPEN_CHECK,
        status: true
    }
}
export const ModalCheckCloseAction = () =>{
    return {
        type: MODAL_OPEN_CHECK,
        status: false
    }
}