// nơi mô tả nhưng sự kiện liên quan tới product 
import { PRODUCTS_FETCH_PENDING, PRODUCTS_FETCH_SUCCESS, PRODUCTS_FETCH_ERROR, PRODUCTS_PAGE_CHANGE, PRODUCT_FETCH_BY_ID_SUCCESS } from "../constants/product.constans";

const fetchAPI = async (url, requestOptions) => {
        let res = await fetch(url, requestOptions);
        let data = await res.json()
        return data
    }


export const getAllProduct =(limit,page)=>{
    return async (dispatch) => {
        await dispatch({
            type: PRODUCTS_FETCH_PENDING
        })

        const responseTotalUser = await fetch("http://localhost:8000/products");

        const dataProduct = await responseTotalUser.json();

        console.log(dataProduct.Data.length)

        const params = new URLSearchParams({
            _start: (page - 1) * limit,
            _limit: limit
        });


        fetchAPI("http://localhost:8000/products" + `?`+ params.toString(),  {method: 'GET',redirect: 'follow'})
                .then(response => {
                    return dispatch ({
                        type: PRODUCTS_FETCH_SUCCESS,
                        length: dataProduct.Data.length,
                        data: response,
                    })
                })
                .catch(err => {
                    return dispatch ({
                        type: PRODUCTS_FETCH_ERROR,
                        error: err
                    })
                })
        
    }
}

export const getProductById = (paramID)=>{
    return async (dispatch) => {
        await dispatch({
            type: PRODUCTS_FETCH_PENDING
        })
        fetchAPI("http://localhost:8000/products/" + paramID,  {method: 'GET',redirect: 'follow'})
                .then(response => {
                    // console.log(response)
                    return dispatch ({
                        type: PRODUCT_FETCH_BY_ID_SUCCESS,
                        productID: response.Data
                    })
                })
                .catch(err => {
                    return dispatch ({
                        type: PRODUCTS_FETCH_ERROR,
                        error: err
                    })
                })
        
    }
}

export const getProductRelated = (related)=>{
    return async (dispatch) => {
        await dispatch({
            type: PRODUCTS_FETCH_PENDING
        })

        const responseTotalUser = await fetch(`http://localhost:8000/products?Type=${related}`);

        const dataProduct = await responseTotalUser.json();

    
        fetchAPI(`http://localhost:8000/products?Type=${related}` ,  {method: 'GET',redirect: 'follow'})
                .then(response => {
                    // console.log(response)
                    return dispatch ({
                        type: PRODUCTS_FETCH_SUCCESS,
                        length: dataProduct.Data.length,
                        data: response
                    })
                })
                .catch(err => {
                    return dispatch ({
                        type: PRODUCTS_FETCH_ERROR,
                        error: err
                    })
                })
        
    }
}

export const changePageAction = (page)=>{
    return {
        type: PRODUCTS_PAGE_CHANGE,
        page
    }
    
}