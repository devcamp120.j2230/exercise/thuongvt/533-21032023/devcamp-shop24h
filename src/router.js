

import PersistentDrawerLeft from "./account/page/my.account.page"
import NotificationAccPage from "./account/page/notification.account.page"
import OderAccPage from "./account/page/order.account.page"
import ProfileAccPage from "./account/page/profile.account.page"
import AdminAcc from "./admin/pages/admin.account.page"
import AdminDiscount from "./admin/pages/admin.discount.page"
import AdminProduct from "./admin/pages/admin.product.page"
import UserManage from "./admin/pages/admin.user.page"
import Administration from "./admin/pages/administration.page"
import OrderManage from "./admin/pages/order.manage.page"
// import Administration from "./admin/pages/administration.page"
import CartPage from "./pages/cartPage"
import CheckOutPage from "./pages/checkOut"
import HomePage from "./pages/homePage"
import ProductInfo from "./pages/ProductInfo"
import ProductList from "./pages/ProductList"

const RouteList = [
    {label:"homePage", path:"/", element:<HomePage></HomePage>},
    {label:"Product list", path:"/products", element:<ProductList></ProductList>},
    {label:"Product info", path:"/products/:productId", element:<ProductInfo></ProductInfo>},
    {label:"Product cart", path:"/cart", element:<CartPage></CartPage>},
    {label:"check out", path:"/checkOut", element:<CheckOutPage></CheckOutPage>},
    {label:"administration", path:"/administration",element:<Administration></Administration>},
    {label:"adminAcc", path:"/adminAcc",element:<AdminAcc></AdminAcc>},
    {label:"orderManage", path:"/orderManage",element:<OrderManage></OrderManage>},
    {label:"UserManage", path:"/UserManage",element:<UserManage></UserManage>},
    {label:"ProductManage", path:"/ProductManage",element:<AdminProduct></AdminProduct>},
    {label:"DiscountManage", path:"/DiscountManage",element:<AdminDiscount></AdminDiscount>},
    {label:"myAccount", path:"/myAccount", element:<PersistentDrawerLeft></PersistentDrawerLeft>},
    {label:"OderAccount", path:"/OrderAccount", element:<OderAccPage></OderAccPage>},
    {label:"ProfileAccount", path:"/ProfileAccount", element:<ProfileAccPage></ProfileAccPage>},
    {label:"NotificationAccount", path:"/NotificationAccount", element:<NotificationAccPage></NotificationAccPage>}
]
export default RouteList