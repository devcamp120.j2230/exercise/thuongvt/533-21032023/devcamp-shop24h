
function ProfileAdminACC() {
    return <>
        <div className="div-style-class">
            <div className="row form-group min-vh-100 text-center">
                <div className="container text-center text-black div-css-div">
                    <p>This is your profile page. You can see the progress you've made with your work and manage your projects or assigned tasks</p>
                    <div className="row" style={{ margin: "20px" }}>
                        <div className="col-lg-4 col-sm-12 col-sx-6 div-style-2">
                            <div className="card-user">
                                <div className="image">
                                    <img src="https://i.imgur.com/w5y5G8b.jpg" alt="..." />
                                </div>

                                <div className="card-body-user">
                                    <div className="author">
                                        <img className="avatar border-gray" src="https://i.imgur.com/w5y5G8b.jpg" alt="..." />
                                        <h5 className="title">Họ và tên</h5>
                                        <p>nơi hiển thị họ và tên</p>
                                        <p className="description">Địa chỉ:</p>
                                        <p className="description">Số điện thoại:</p>
                                        <p className="description">Email:</p>
                                        <p className="description">Fecebook:</p>
                                    </div>

                                </div>
                                <hr></hr>
                                <div className="card-footer-user">
                                    <p>Thông tin tài khoản tài khoản</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-8 col-sm-12 col-sx-6 div-style-1">
                            <div className="card-user-profile">
                                <div className="card-header-profile">
                                    <h5 className="card-title">Edit Profile</h5>
                                </div>
                                <div className="card-body-profile">
                                    <form>
                                        <div>
                                            <div className="row" >
                                                <div className="form-group col-md-6">
                                                    <label for="" className="mb-0">First Name</label>
                                                    <input type="text" className="form-control" placeholder="Enter First Name"  ></input>
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label for="" className="mb-0">Last Name</label>
                                                    <input type="text" className="form-control" placeholder="Enter Last Name" ></input>
                                                </div>
                                            </div>
                                            <div className="row" style={{ marginTop: "20px" }}>
                                                <div className="form-group col-md-6">
                                                    <label for="" class="mb-0">Phone</label>
                                                    <input type="email" className="form-control" placeholder="Enter Phone" ></input>
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label for="" className="mb-0">Link image</label>
                                                    <input type="text" className="form-control" placeholder="Enter Link" ></input>
                                                </div>
                                            </div>
                                            <div className="row" style={{ marginTop: "20px" }}>
                                                <div className="form-group col-md-6">
                                                    <label for="" class="mb-0">Town / City(*)</label>
                                                    <select name="" placeholder="Choose City" class="form-control" >

                                                    </select>
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label for="" className="mb-0">District/province(*)</label>
                                                    <select name="" placeholder="Choose District" className="form-control" >

                                                    </select>
                                                </div>
                                            </div>
                                            <div className="row" style={{ marginTop: "20px" }}>
                                                <div className="form-group col-md-6">
                                                    <label for="" className="mb-0">Sub-district/ wards(*)</label>
                                                    <select name="" placeholder="Choose Commune" className="form-control" >

                                                    </select>
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label for="" className="mb-0">Street/Hamlet(*)</label>
                                                    <input type="text" className="form-control" placeholder="Street/Hamlet" ></input>
                                                </div>
                                            </div>
                                            <div className="row" style={{ marginTop: "20px" }}>
                                                <div className="form-group col-md-6">
                                                    <label for="" class="mb-0">Email Address</label>
                                                    <input type="email" className="form-control" placeholder="Enter Email"  ></input>
                                                </div>
                                                <div className="form-group col-md-6">
                                                    <label for="" className="mb-0">Password</label>
                                                    <input type="password" className="form-control" placeholder="Password" ></input>
                                                </div>
                                            </div>
                                            <hr></hr>
                                            <button type="submit" className="btn-upload" style={{ marginTop: "20px" }}>Update Profile</button>

                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <hr></hr>
        <footer className="footer-profile">
            <div className="container-fluid">
                <div className="row-footer">
                    <nav class="footer-nav">
                        <ul>
                            <li>Creative thuongvt</li>
                            <li>Blog</li>
                            <li>Licenses</li>
                        </ul>
                    </nav>
                    <div className="credits ml-auto">
                        <span className="copyright">
                            © <script>
                            </script>, made with <i class="fa fa-heart heart"></i> by Creative Thuongvt
                        </span>
                    </div>
                </div>
            </div>
        </footer>
    </>
}
export default ProfileAdminACC