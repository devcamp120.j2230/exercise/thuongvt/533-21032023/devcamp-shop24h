import "bootstrap/dist/css/bootstrap.min.css";
import { onAuthStateChanged } from "firebase/auth";
import { useDispatch } from "react-redux";
import { Route, Routes } from "react-router-dom";
import './App.css';
import HomePage from "./pages/homePage";
import RouteList from "./router";
import auth from "./component/firebase/config";
import { LOGIN_FETCH_ERROR, SAVE_FETCH_SUCCESS } from "./constants/login.constans";
import { useEffect } from "react";
function App() {
  // khởi tạo gg auth provider
  const dispatch = useDispatch()
  const fetchAPI = async (url, requestOptions) => {
    let res = await fetch(url, requestOptions);
    let data = await res.json()
    return data
  }
  const IdUser = localStorage.getItem("Id")

  const getUser = () => {
   
      fetchAPI("http://localhost:8000/customers/" + IdUser, { method: 'GET', redirect: 'follow' })
        .then(response => {
          // console.log(response)
          return dispatch ({
            type: SAVE_FETCH_SUCCESS,
            payload: response.Data
          })
        })
        .catch(err => {
          console.log("lỗi rồi")
          return dispatch ({
              type: LOGIN_FETCH_ERROR,
              error: err
          })
        })
    
  }
  useEffect(() => {
    getUser()


    //Lưu trữ thông tin đăng nhập khi load lại trang
    // onAuthStateChanged(auth, (result) => {
    //   dispatch({
    //     type: SAVE_FETCH_SUCCESS,
    //     payload: result
    //   })
    // })



  }, [])

  return (
    <>
      <Routes>
        {RouteList.map((router, index) => {
          if (router.path) {
            return <Route key={index} path={router.path} element={router.element}>
            </Route>
          }
          else {
            return null
          }
        })}
        <Route path="*" element={<HomePage></HomePage>}></Route>
      </Routes>
    </>
  );
}

export default App;
