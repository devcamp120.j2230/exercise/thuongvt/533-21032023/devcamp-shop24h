import { ADD_TOTAL_ERROR, ADD_TOTAL_SUCCESS } from "../constants/total.constans";

const cartState = {
    subtotal: 0,
    discount: 0,
    total: 0
}



 const cartReducer = (state = cartState, action)=>{
    switch (action.type) {
        case ADD_TOTAL_ERROR:
            state.subtotal = 0;
            break;
        case ADD_TOTAL_SUCCESS:
            state.subtotal = action.data;
            break;
        default:
            break;
    }
    state.total = state.subtotal - state.discount
    return {...state}
}

export default cartReducer