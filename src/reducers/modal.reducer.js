import { MODAL_OPEN_CHECK, MODAL_OPEN_lOGIN } from "../constants/modal.constans"

const ModalLogInState ={
    openModalLogin: false,
    openModalCheck:false
}

const ModalLogInReducer = (state = ModalLogInState, action) => {
        switch (action.type){
            case MODAL_OPEN_lOGIN:
                state.openModalLogin = action.status
            break;
            case MODAL_OPEN_CHECK:
                state.openModalCheck = action.status
            break;
        }
        return{...state}
}

export default ModalLogInReducer