import { ADD_PRODUCT_ERROR, ADD_PRODUCT_PENDING, ADD_PRODUCT_SUCCESS } from "../constants/cart.constans";

const addProductState = {
    pending:false,
    addProduct: ""
}

const addProductReducer = (state= addProductState, action)=>{
    switch (action.type) {
        case ADD_PRODUCT_PENDING:
            state.pending = true;
            break;
        case ADD_PRODUCT_SUCCESS:
            state.pending = false;
            state.addProduct = action.data
            break;
        case ADD_PRODUCT_ERROR:
            state.pending = true;
            break;
        default:
            break;
    }
    return {...state}
}
export default addProductReducer