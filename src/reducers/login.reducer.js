import { LOGIN_EMAIL_ERROR, LOGIN_EMAIL_SUCCESS, LOGIN_FETCH_ERROR, LOGIN_FETCH_PENDING, LOGIN_FETCH_SUCCESS, LOGOUT_EMAIL_SUCCESS, LOGOUT_FETCH_SUCCESS, SAVE_FETCH_SUCCESS } from "../constants/login.constans";

const loginSate = {
    userGG:"",
    user: "",
    userAccessToken:"",
    pending: false
}

const loginReducer = (state = loginSate, action) => {
    switch (action.type) {
        case LOGIN_FETCH_PENDING:
            state.pending = true;
            break;
        case LOGIN_FETCH_SUCCESS:
            state.pending = false;
            state.userGG = action.payload
            break;
        case LOGIN_EMAIL_SUCCESS:
            state.pending =false;
            state.user=action.payload
            state.userAccessToken=action.token
            break;
        case LOGIN_FETCH_ERROR:
            state.pending = true;
            break;
        case LOGIN_EMAIL_ERROR:
            state.pending = true;
            break;
        case LOGOUT_FETCH_SUCCESS:
            state.userGG = null
            break;
        case LOGOUT_EMAIL_SUCCESS:
            state.user = null
            break;    
        case SAVE_FETCH_SUCCESS:
            state.pending = false;
            state.user = action.payload
            break;
        default:
            break;
    }
    return { ...state }
}

export default loginReducer