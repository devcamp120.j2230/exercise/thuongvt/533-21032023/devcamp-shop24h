// root reducer chứ reducer chính
import { combineReducers } from "redux";
import productReducer from "./product.reducer";
import loginReducer from "./login.reducer";
import addProductReducer from "./cart.reducers";
import cartReducer from "./total.reducers";
import ModalLogInReducer from "./modal.reducer";
const RootReducer = combineReducers({
    productReducer, loginReducer, addProductReducer, cartReducer, ModalLogInReducer
});
export default RootReducer