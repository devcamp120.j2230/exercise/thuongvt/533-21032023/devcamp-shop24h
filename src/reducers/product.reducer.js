import { PRODUCTS_FETCH_ERROR, PRODUCTS_FETCH_PENDING, PRODUCTS_FETCH_SUCCESS, PRODUCTS_PAGE_CHANGE, PRODUCT_FETCH_BY_ID_SUCCESS } from "../constants/product.constans"

const productState = {
    products:[],
    pending: false,
    limitProduct: 9,
    currentPage: 1,
    countPage: 0,
    productById: []
}

const productReducer =(state = productState,action)=>{
    switch (action.type){
        case PRODUCTS_FETCH_PENDING :
            state.pending= true;
            break;
        case PRODUCTS_FETCH_SUCCESS:
            state.pending= false;
            state.countPage =  Math.ceil(action.length / state.limitProduct);
            state.products = action.data.Data.slice((state.currentPage-1)*state.limitProduct,(state.currentPage*state.limitProduct))
            break;
        case PRODUCTS_FETCH_ERROR:
            state.pending= true;
            break;
        case PRODUCTS_PAGE_CHANGE:
            state.currentPage = action.page
            break;
        case PRODUCT_FETCH_BY_ID_SUCCESS:
            state.productById = action.productID
            break;
        default:
            break;

    }
    return {...state}
}
export default productReducer