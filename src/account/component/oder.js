
function Order() {
    return <>
        <div className="row form-group min-vh-100">
            <div className="container">
                <div className="text-center">
                    <h3>Hello user</h3>
                    <p>Welcome to your order page</p>
                </div>
                <div className="row mb-4">
                    <div className="col-lg-12 col-sm-12 col-sx-6  div-style-2">
                        <div className="card-order container ">
                            <ul className="timeline-order">
                                <li>
                                    <div className="timeline-img">
                                        <img src="https://i.imgur.com/Oi9r9HL.jpg" alt="..."></img>
                                    </div>
                                    <div className="timeline-panel">Trang chủ</div>
                                </li>
                                <li>
                                    <div className="timeline-img">
                                        <img src="https://i.imgur.com/teMxGEs.png" alt="..."></img>
                                    </div>
                                    <div className="timeline-panel">Big sale</div>
                                </li>
                                <li>
                                    <div className="timeline-img">
                                        <img src="https://i.imgur.com/m1FPpAH.jpg" alt="..."></img>
                                    </div>
                                    <div className="timeline-panel">Free ship</div>
                                </li>
                                <li>
                                    <div className="timeline-img">
                                        <img src="https://i.imgur.com/qLgV2WD.jpg" alt="..."></img>
                                    </div>
                                    <div className="timeline-panel">Sale</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row text-center" >
                        <div className="col-lg-7 col-sm-12 col-sx-6 order">
                            <div className="text-center mt-3 ">
                                <p style={{ fontWeight: "600" }}>Đơn mua</p>
                                <div>
                                    <table className="table" style={{ textAlign: "center", marginTop: "10px" }}>
                                        {/* header */}
                                        <thead style={{ backgroundColor: "#d33b33" }}>
                                            <tr style={{ color: "white" }}>
                                                <th>Images</th>
                                                <th>Product Name</th>
                                                <th>Price</th>
                                                <th>count</th>
                                            </tr>
                                        </thead>
                                        {/* body */}
                                        {/* <tbody>
                                    {data.map((e, i) => {
                                        return <tr key={i}>
                                            <td className="thumbnail-img">
                                                <a>
                                                    <img className="img-fluid" src={e.imageUrl} alt={`${e.name}`} style={{ width: "40px" }} />
                                                </a>
                                            </td>
                                            <td className="name-pr" >
                                                <p style={{ marginTop: "10px" }}>
                                                    {e.name}
                                                </p>
                                            </td>
                                            <td className="price-pr">
                                                <p style={{ marginTop: "10px" }}>$:{e.buyPrice} </p>
                                            </td>
                                            <td className="total-pr">
                                                <p style={{ marginTop: "10px" }} >{e.count}</p>
                                            </td>
                                        </tr>
                                    })}
                                </tbody> */}
                                    </table>
                                </div>
                            </div>

                        </div>
                        <div className="col-lg-5 col-sm-12 col-sx-6 delivery-status">
                            <div className="text-center mt-3">
                                <p style={{ fontWeight: "600" }}>Tình trạng giao hàng</p>
                            </div>
                            <div>
                                <ul className="timeline">
                                    <li className="event" data-date="12:30 - 1:00pm">
                                        <h3>Hàng đang được chuẩn bị</h3>
                                        <p>Hàng đang được người bán chuẩn bị</p>
                                    </li>
                                    <li className="event" data-date="2:30 - 4:00pm">
                                        <h3>Tới trạm trung chuyển</h3>
                                        <p>Hàng đã được nhập tại kho trung chuyển</p>
                                    </li>
                                    <li className="event" data-date="5:00 - 8:00pm">
                                        <h3>Hàng rời kho phân loại</h3>
                                        <p>Hàng đã rời kho phân loại HN SOC</p>
                                    </li>
                                    <li className="event" data-date="8:30 - 9:30pm">
                                        <h3>Đang trên đường giao tới bạn</h3>
                                        <p>Đang vận chuyển tới bạn</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <hr></hr>
        <footer className="footer-profile mt-2">
            <div className="container-fluid">
                <div className="row-footer">
                    <nav class="footer-nav">
                        <ul>
                            <li>Creative thuongvt</li>
                            <li>Blog</li>
                            <li>Licenses</li>
                        </ul>
                    </nav>
                    <div className="credits ml-auto">
                        <span className="copyright">
                            © <script>
                            </script>, made with <i class="fa fa-heart heart"></i> by Creative Thuongvt
                        </span>
                    </div>
                </div>
            </div>
        </footer>
    </>
}
export default Order