import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import HomeIcon from '@mui/icons-material/Home';
import MessageIcon from '@mui/icons-material/Message';
import WalletIcon from '@mui/icons-material/Wallet';
import LocalMallSharpIcon from '@mui/icons-material/LocalMallSharp';
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import { useSelector } from 'react-redux';
import Order from '../component/oder';


const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: `-${drawerWidth}px`,
        ...(open && {
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
            marginLeft: 0,
        }),
    }),
);

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
    transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: `${drawerWidth}px`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));

export default function PersistentDrawerLeft() {



    const theme = useTheme();
    const [open, setOpen] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const {user} = useSelector((reduxLogIn) => {
        return reduxLogIn.loginReducer
    })


    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar position="fixed" open={open} style={{backgroundColor:"#d33b33"}}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        sx={{ mr: 2, ...(open && { display: 'none' }) }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap component="div">
                    My account
                    </Typography>
                </Toolbar>
            </AppBar>
            <Drawer
                sx={{
                    width: drawerWidth,
                    flexShrink: 0,
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                    },
                }}
                variant="persistent"
                anchor="left"
                open={open}
            >
                <DrawerHeader>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                </DrawerHeader>
                <Divider />
                <List>
                    <ListItemButton href="/ProfileAccount">
                        <ListItemIcon>
                            <Typography component="div"><img src={user.image} alt={""} style={{ width: "50px", borderRadius: "50%", marginRight: "10px", marginBottom: "10px" }} ></img> {user.firstName} </Typography>
                        </ListItemIcon>
                        <ListItemText />
                    </ListItemButton>
                </List>
                <Divider />
                <List>
                    <ListItemButton href="/">
                        <ListItemIcon>
                            <Typography><HomeIcon></HomeIcon> Trang chủ </Typography>
                        </ListItemIcon>
                        <ListItemText />
                    </ListItemButton>
                </List>
                <List>
                    <ListItemButton href="/OrderAccount">
                        <ListItemIcon>
                            <Typography component="div"><LocalMallSharpIcon></LocalMallSharpIcon> Đơn hàng </Typography>
                        </ListItemIcon>
                        <ListItemText />
                    </ListItemButton>
                </List>
                <List>
                    <ListItemButton href="/NotificationAccount">
                        <ListItemIcon>
                            <Typography component="div"><NotificationsNoneIcon></NotificationsNoneIcon> Thông báo</Typography>
                        </ListItemIcon>
                        <ListItemText />
                    </ListItemButton>
                </List>
                <List>
                    <ListItemButton>
                        <ListItemIcon>
                            <Typography component="div"><MessageIcon></MessageIcon> Tin nhắn </Typography>
                        </ListItemIcon>
                        <ListItemText />
                    </ListItemButton>
                </List>
                <List>
                    <ListItemButton>
                        <ListItemIcon>
                            <Typography component="div"><WalletIcon></WalletIcon> Ví </Typography>
                        </ListItemIcon>
                        <ListItemText />
                    </ListItemButton>
                </List>
            </Drawer>
            
            <Main open={open} style={{backgroundColor:"antiquewhite"}}>
                <DrawerHeader />
                <Order></Order>
            </Main>
        </Box>
    );
}