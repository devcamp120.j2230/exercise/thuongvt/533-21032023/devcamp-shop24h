import { Grid } from "@mui/material"
import Header1 from "../component/header1/header"
import BreadCrumb from "../component/breadCrumb/BreadCrumb"
import ProductImage from "../component/products/ProductImage"
import ProductImageFooter from "../component/products/ProductImageFooter"
import Footer1 from "../component/footer/footer"
import CheckOutContent from "../component/checkOuts/check.out.content"
const breadCrumb = [
    {
        name: "Home Page",
        url: "/"
    },
    {
        name: "Product List",
        url: "/products"
    },
    {
        name: "cart",
        url: "/cart"
    },
    {
        name: "check out",
        url: "/checkOut"
    },

] 
function CheckOutPage () {
    return (
        <Grid>
            <Grid item><Header1></Header1></Grid>
            <Grid item><BreadCrumb link={JSON.stringify(breadCrumb)}></BreadCrumb></Grid>
            <ProductImage data = {{Name:"Check out"}}></ProductImage>
            <CheckOutContent></CheckOutContent>
            <ProductImageFooter></ProductImageFooter>
            <Footer1></Footer1>
        </Grid>
    )
}

export default CheckOutPage