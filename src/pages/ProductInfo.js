import { Container, Grid } from "@mui/material"
import { useParams } from "react-router-dom"
import BreadCrumb from "../component/breadCrumb/BreadCrumb"
import Footer1 from "../component/footer/footer"
import Header1 from "../component/header1/header"
import { useEffect } from "react"
import ShowData from "../component/product-detail/product.detail.show.data"
import ProductRelated from "../component/product-detail/product.related.data"
import { useDispatch, useSelector } from "react-redux"
import { getProductById } from "../actions/product.action"
import ProductImage from "../component/products/ProductImage"
import ProductImageFooter from "../component/products/ProductImageFooter"
const breadCrumb = [
    {
        name: "Home Page",
        url: "/"
    },
    {
        name: "Product List",
        url: "/products"
    },
    {
        name: "Product Info",
        url: "#"
    }
]

function ProductInfo() {

    const { productId } = useParams()
    // console.log(productId)

    const dispatch = useDispatch()

    const { productById } = useSelector((reduxData) => {
        return reduxData.productReducer
    })
    

    useEffect(() => {
        dispatch(getProductById(productId))
    }, [productId])

    // console.log(productById)
    return (
        <>
            <Header1></Header1>
            <BreadCrumb link={JSON.stringify(breadCrumb)} ></BreadCrumb>
            <ProductImage data = {{Name:"Product Info"}}></ProductImage>
            <Container margin={3}>
                <ShowData data={productById}></ShowData>
            </Container>

            <Grid>
                <ProductRelated data={productById}></ProductRelated>
            </Grid>
            <ProductImageFooter></ProductImageFooter>
            <Footer1></Footer1>
        </>
    )
}

export default ProductInfo