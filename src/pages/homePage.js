import Content from "../component/content/content";
import Footer1 from "../component/footer/footer";
import Header1 from "../component/header1/header";
import BreadCrumb from "../component/breadCrumb/BreadCrumb";
import {  Grid } from "@mui/material";

const breadCrumb = [
    {
        name: "Home Page",
        url: "/"
    },

]
function HomePage() {
    return (
        <Grid component="container">
            <Grid item><Header1></Header1></Grid>
            <Grid item><BreadCrumb link={JSON.stringify(breadCrumb)}></BreadCrumb></Grid>
            <Grid item><Content></Content></Grid>
            <Grid item><Footer1></Footer1></Grid>
        </Grid>
    )
}
export default HomePage