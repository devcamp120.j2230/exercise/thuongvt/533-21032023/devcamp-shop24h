import { Grid } from "@mui/material"
import BreadCrumb from "../component/breadCrumb/BreadCrumb"
import CartContent from "../component/carts/cart.content"
import Footer1 from "../component/footer/footer"
import Header1 from "../component/header1/header"
import ProductImage from "../component/products/ProductImage"
import ProductImageFooter from "../component/products/ProductImageFooter"
const breadCrumb = [
    {
        name: "Home Page",
        url: "/"
    },
    {
        name: "Product List",
        url: "/products"
    },
    {
        name: "cart",
        url: "/cart"
    },

] 


function CartPage() {
    
    return (<>
        {
            localStorage.length ?
                <Grid>  
                    <Header1></Header1>
                    <BreadCrumb link={JSON.stringify(breadCrumb)}></BreadCrumb>
                    <ProductImage data = {{Name:"Cart"}}></ProductImage>
                    <CartContent></CartContent>
                    <ProductImageFooter></ProductImageFooter>
                    <Footer1></Footer1>
                </Grid> :
                <Grid>
                    <Header1></Header1>
                    <BreadCrumb link={JSON.stringify(breadCrumb)}></BreadCrumb>
                    <ProductImage data = {{Name:"Cart"}}></ProductImage>
                    <CartContent></CartContent>
                    <ProductImageFooter></ProductImageFooter>
                    <Footer1></Footer1>
                </Grid>
        }

    </>)
}

export default CartPage