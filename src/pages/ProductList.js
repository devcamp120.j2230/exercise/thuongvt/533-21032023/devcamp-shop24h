import { Container, Grid } from "@mui/material"
import BreadCrumb from "../component/breadCrumb/BreadCrumb"
import Footer1 from "../component/footer/footer"
import Header1 from "../component/header1/header"
import ProductCart from "../component/products/ProductCart";
import ProductFilter from "../component/products/ProductFilter";
import ProductImage from "../component/products/ProductImage";
import ProductImageFooter from "../component/products/ProductImageFooter";

const breadCrumb = [
    {
        name: "Home Page",
        url: "/"
    },
    {
        name: "Product List",
        url: "/products"
    }

]
function ProductList() {

    return (
        <Grid>
            <Grid item><Header1></Header1></Grid>
            <Grid item><BreadCrumb link={JSON.stringify(breadCrumb)}></BreadCrumb></Grid>
            <Grid item><ProductImage data = {{Name:"Product list"}}></ProductImage></Grid>
                    <Container>
                        <Grid container>
                            <Grid item xs={2} mt={4}>
                                <ProductFilter></ProductFilter>
                            </Grid>
                            <Grid item xs={10} mt={3} >
                                <ProductCart></ProductCart>
                            </Grid>
                        </Grid>
                    </Container>
            <Grid item><ProductImageFooter></ProductImageFooter></Grid>
            <Grid item><Footer1></Footer1></Grid>
        </Grid>
    )
}
export default ProductList