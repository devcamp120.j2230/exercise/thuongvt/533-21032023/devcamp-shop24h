import { Button } from "@mui/material"
import DeleteIcon from '@mui/icons-material/Delete';
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getItemProduct } from "../../actions/cart.action";
import { getSubTotal } from "../../actions/total.actions";

function CartItem(props) {

    // Giá trị đucợ truyền qua từ một mảng có trong local store
    const dataProduct = props.item
    const dispatch = useDispatch()
    // lấy giá trị subtotal từ reducer
    const { subtotal } = useSelector((reduxData) => {
        return reduxData.cartReducer
    })
    const [count, setCount] = useState(dataProduct.count)



    const addProductCart = () => {
        setCount(count + 1);
        // // nếu có người dùng cộng thêm số lượng sẽ cộng thêm vào subtotal như một vòng tròn
        dispatch(getSubTotal(subtotal + dataProduct.buyPrice))

        // Tạo mảng chứa phần tử cần lưu trữ
        var cartArray = []
        //lấy danh sách sản phẩm từ localstorage
        cartArray = localStorage.getItem("Product") ? JSON.parse(localStorage.getItem("Product")) : [];
        //kiểm tra sản phẩm đã được thêm vào cart chưa?
        //nếu chưa thì thêm vào cart
        var check = -1;

        if (cartArray.length !== 0) {
            cartArray.find((item, key) => {
                if (item._id === dataProduct._id) {
                    check = key
                }
            });
        }

        if (check < 0) {
            cartArray.push({ ...dataProduct, count });
        } else {
            cartArray[check].count += 1;
        }

        //thêm thay đổi vào localstorage
        localStorage.setItem("Product", JSON.stringify(cartArray));
    }

    const removeProductCart = () => {
        //giá trị của count bắt buộc lớn hơn không nếu không sẽ mặc định bằng 0
        if (count > 0) {
            setCount(count - 1);
            //Nếu có người dùng trừ đi số lượng sẽ trừ vào subtotal như một vòng tròn
            dispatch(getSubTotal(subtotal - dataProduct.buyPrice))

            //Tạo mảng chứa phần tử cần lưu trữ
            var cartArray = []
            //lấy danh sách sản phẩm từ localstorage
            cartArray = localStorage.getItem("Product") ? JSON.parse(localStorage.getItem("Product")) : [];
            //Kiểm tra sản phẩm đã được thêm vào cart chưa?
            //Nếu chưa thì thêm vào cart
            var check = -1;

            if (cartArray.length !== 0) {
                cartArray.find((item, key) => {
                    if (item._id === dataProduct._id) {
                        check = key
                    }
                });
            }

            if (check < 0) {
                cartArray.push({ ...dataProduct, count });
            } else {
                cartArray[check].count -= 1;
            }

            //thêm thay đổi vào localstorage
            localStorage.setItem("Product", JSON.stringify(cartArray));

        }
        else {
            setCount(0)
        }
    }



    // hàm dùng xóa sản phẩm trong giỏ hàng
    const deleteProduct = (param) => {
        // tạo mảng chứa các phần tử trong localstorge
        var dataLocalStorage = []
        // Kiểm tra xem có giá trị hay không
        dataLocalStorage = localStorage.getItem("Product") ? JSON.parse(localStorage.getItem("Product")) : []
        // Duyệt phần tử trong mảng tìm phần tử trong mảng có id trùng với id cần xóa
        var index = null

        if (dataLocalStorage.length !== 0) {
            dataLocalStorage.find((item, i) => {
                if (item._id === param._id) {
                    index = i
                }
            })
        }
        if (index !== null) {
            //tiến hành xóa phần tử trong mảng
            dataLocalStorage.splice(index, 1)
        }
        //thêm thay đổi vào localstorage
        localStorage.setItem("Product", JSON.stringify(dataLocalStorage));
        //goi hàm thay đổi cập nhật lại số phần tử trong mảng data local storage khi xóa phần tử khỏi giỏ hàng
        dispatch(getItemProduct(dataLocalStorage.length))
    }


    return (
        <tr>
            <td className="thumbnail-img">
                <a>
                    <img className="img-fluid" src={dataProduct.imageUrl} alt={`${dataProduct.name}`} style={{ width: "80px" }} />
                </a>
            </td>
            <td className="name-pr" >
                <p style={{ marginTop: "20px" }}>
                    {dataProduct.name}
                </p>
            </td>
            <td className="price-pr">
                <p style={{ marginTop: "20px" }}>$: {dataProduct.buyPrice}</p>
            </td>
            <td  >
                <p style={{ marginTop: "15px" }}>

                    <Button onClick={addProductCart}> + </Button>
                    {dataProduct.count}
                    <Button onClick={removeProductCart}>-</Button>
                </p>

            </td>
            <td className="total-pr">
                <p style={{ marginTop: "20px" }} >$: {dataProduct.buyPrice*dataProduct.count}</p>
            </td>
            <td className="remove-pr">
                <Button style={{ marginTop: "15px" }} onClick={() => { deleteProduct(dataProduct) }}><DeleteIcon></DeleteIcon></Button>
            </td>
        </tr>
    )
}

export default CartItem