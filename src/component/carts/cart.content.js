
import { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getSubTotal } from '../../actions/total.actions';
import CartItem from './cart.item';
import { useNavigate } from 'react-router-dom';


export default function CartContent() {

  const navigator = useNavigate()
  const dispatch = useDispatch()
  const [productAdd, setProductAdd] = useState([])

  // lấy giá trị trong này để khi thay đổi localStore để khi có sự thay đổi thì load lại trang
  const { addProduct } = useSelector((reduxLogIn) => {
    return reduxLogIn.addProductReducer
  })


  // lấy các giá trị trong total reducer để hiển thị trang
  const { subtotal, discount, total } = useSelector((reduxTotal) => {
    return reduxTotal.cartReducer
  })


  // sự kiện khi bấm nút check out
  const btnCheckOut = () => {
    navigator(`/checkOut`)
  }



  //Load lại loacal store khi có sự kiện thay đổi
  useEffect(() => {
    // lấy thông tin từ local storage
    const item = JSON.parse(localStorage.getItem("Product"));
    // console.log(item)
    var total = 0;
    if (item) {
      // set giá trị cho mảng mới để dừng hàm map đẩy giá trị cho giỏ hàng
      setProductAdd(item);
      // sử dụng hàm duyệt qua mảng cộng tất cả các Price trong mảng sau đó set giá trị cho subTotal 
      item.map((item, index) => {
        total += item.buyPrice * item.count
      });
      dispatch(getSubTotal(total))
    }
    // hàm này sẽ load lại khi có thêm hàng mới
  }, [addProduct, subtotal]);


  return (
    <>
      <div className='container' style={{ marginTop: "20px" }}>
        <table className="table" style={{ textAlign: "center" }}>
          {/* header */}
          <thead style={{ backgroundColor: "#d33b33" }}>
            <tr style={{ color: "white" }}>
              <th>Images</th>
              <th>Product Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Total</th>
              <th>Remove</th>
            </tr>
          </thead>
          {/* body */}
          <tbody>
            {productAdd.map((e, i) => {
              return <Fragment key={i}>
                <CartItem item={e}></CartItem>
              </Fragment>
            })}
          </tbody>
        </table>
        <>
          <div className="row mt-5" style={{ textAlign: "end" }}>
            <div className="col-lg-8 col-sm-8">
              <div className="coupon-box">
                <div className="input-group">
                  <input className="form-control" placeholder="Enter your coupon code" aria-label="Coupon code" type="text" />
                  <div className="input-group-append">
                    <input value="Apply Coupon" type="submit" className="form-control" style={{ backgroundColor: "black", color: "white" }} />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-sm-4">
              <div className="update-box">
                <input value="Update Cart" type="submit" className="form-control" style={{ backgroundColor: "black", color: "white" }} />
              </div>
            </div>
          </div>
        </>
      </div>

      {/* total pirce */}
      <div className='container'>
        <div className="row my-5">
          <div className="col-lg-8 col-sm-12 col-sx-6"></div>
          <div className="col-lg-4 col-sm-12 col-sx-6">
            <div>
              <h4 style={{ fontSize: "20px" }}>Order summary</h4>
              <div className="d-flex" style={{ display: "flex", justifyContent: "space-between" }} >
                <p>Sub Total </p>
                <div className="font-weight-bold" ><p style={{ marginLeft: "30px" }}>$: {subtotal} </p> </div>
              </div>
              <div className="d-flex" style={{ display: "flex", justifyContent: "space-between" }} >
                <p> Discount </p>
                <div className="font-weight-bold" ><p style={{ marginLeft: "30px" }}>$: {discount} </p> </div>
              </div>
              <hr></hr>
              <div className="d-flex" style={{ display: "flex", justifyContent: "space-between" }}>
                <p>Shipping Cost</p>
                <div className="font-weight-bold" ><p style={{ marginLeft: "30px" }}>$: {""} </p> </div>
              </div>
              <hr></hr>
              <div className="d-flex" style={{ display: "flex", justifyContent: "space-between" }}>
                <span className="text-danger fw-bold" style={{ fontSize: "20px" }}>Grand Total</span>
                <div className="text-danger font-weight-bold" ><span style={{ marginLeft: "30px" }}>$: {total} </span> </div>
              </div>
              <hr></hr>
              <div style={{ textAlign: "right" }}>
                <button type="button" className="btn" style={{ backgroundColor: "#d33b33", color: "white" }} onClick={btnCheckOut} >Checkout</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
