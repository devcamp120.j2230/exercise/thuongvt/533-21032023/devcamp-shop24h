// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCpDkG1vBUDilrSzZKMTYxrxj5O2d8Z6WQ",
  authDomain: "thuongvt123456-1fcdd.firebaseapp.com",
  projectId: "thuongvt123456-1fcdd",
  storageBucket: "thuongvt123456-1fcdd.appspot.com",
  messagingSenderId: "668373701860",
  appId: "1:668373701860:web:925260c349a3efb4bc08f1"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

export default auth