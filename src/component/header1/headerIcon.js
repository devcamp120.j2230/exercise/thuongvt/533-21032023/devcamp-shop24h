import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import LogoutIcon from '@mui/icons-material/Logout';
import LoginIcon from '@mui/icons-material/Login';
import { useEffect, useState } from 'react';
import { Container, Dropdown } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { LoginOutWithEmail } from '../../actions/login.action';
import { Grid, Button } from '@mui/material';
import Badge from '@mui/material/Badge';
import ModalLogIn from '../model/modal.login';
import { ModalLogInOpenAction } from '../../actions/modal.action';


function notificationsLabel(count) {
    if (count === 0) {
        return 'no notifications';
    }
    if (count > 99) {
        return 'more than 99 notifications';
    }
    return `${count} notifications`;
}

function HeaderIcon() {

    const dispatch = useDispatch()

    const [itemLocalStore, setItemLocalStore] = useState()
    // lấy giá trị số phần tử trong localstore để quy định sự hiển thị trên giỏ hàng khi có thay đổi về số lượng
    const { addProduct } = useSelector((reduxLogIn) => {
        return reduxLogIn.addProductReducer
    })
    //Login logOut xác nhận người dùng
    const { user,userAccessToken } = useSelector((reduxLogIn) => {
        return reduxLogIn.loginReducer
    })
    
    const logOutGoogle = () => {
        dispatch(logOut())
    }
    // logout 
    const logOut = ()=>{
        // xóa id được luu trong localstore, đồnng thời logout
        dispatch(LoginOutWithEmail(userAccessToken))
        localStorage.removeItem("Id")
    }

    const logIn = () => {
        dispatch(ModalLogInOpenAction())
    }

    useEffect(() => {
        // lấy giá trị trong 
        setItemLocalStore(JSON.parse(localStorage.getItem("Product")));
    }, [addProduct])

    return (
        <Container style={{ marginTop: "10px" }}>
            <Grid display="flex" flexWrap={"wrap"}>
                {
                    itemLocalStore ?
                        <><Button href="/cart" aria-label={notificationsLabel(100)} ><Badge badgeContent={itemLocalStore.length} color="secondary" ><AddShoppingCartIcon sx={{ cursor: "pointer" }} style={{ backgroundColor: "none" }}></AddShoppingCartIcon></Badge></Button></>
                        : <><Button href="/cart" aria-label={notificationsLabel(100)}><Badge badgeContent={0} color="secondary"><AddShoppingCartIcon sx={{ cursor: "pointer" }} style={{ backgroundColor: "none" }}></AddShoppingCartIcon></Badge></Button></>
                }
                
                {user ? <Dropdown>
                    <Dropdown.Toggle variant="none">
                        <img src={user.image} alt={""} style={{ width: "23px", borderRadius: "50%", marginRight: "10px", marginBottom: "10px" }} ></img>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                        <Dropdown.Item ><p style={{ fontSize: "15px" }}><img src={user.image} alt={""} style={{ width: "23px", borderRadius: "50%", marginRight: "10px" }} ></img>{user.firstName}</p></Dropdown.Item>
                        <Dropdown.Item onClick={logOut} ><LogoutIcon></LogoutIcon> LogOut</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown> :
                    <Dropdown>
                        <Dropdown.Toggle variant="none">
                            <AccountCircleIcon></AccountCircleIcon>
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                            <Dropdown.Item onClick={logIn}><LoginIcon></LoginIcon> Login</Dropdown.Item>
                            {/* <Dropdown.Item onClick={logOutGoogle} >LogOut</Dropdown.Item> */}
                        </Dropdown.Menu>
                    </Dropdown>}


            </Grid>
            <ModalLogIn></ModalLogIn>
        </Container>
    )
}

export default HeaderIcon