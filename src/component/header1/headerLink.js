
import { Grid } from "@mui/material"
import { Col, Nav } from "react-bootstrap"
import { useSelector } from "react-redux"


function HeaderLink() {

    const { user } = useSelector((reduxLogIn) => {
        return reduxLogIn.loginReducer
    })
    // console.log(user)
    return (
        <Col style={{ marginTop: "7px", display: "flex", justifyContent: "center", flexWrap: "wrap" }}>
            <Nav.Link href="/" style={{ marginLeft: "20px", marginRight: "20px", marginTop: "2px" }}><h6>Home</h6></Nav.Link>
            <Nav.Link href="" style={{ marginLeft: "20px", marginRight: "20px", marginTop: "2px" }}><h6>About Us</h6></Nav.Link>
            <Grid href="" style={{ marginLeft: "20px", marginRight: "20px" }}>
                <Grid className="dropdown">
                    <h6 className="drop-btn">Product</h6>
                    <Grid className="dropdown-content">
                        <Nav.Link href="/products">Shoe</Nav.Link>
                        <Nav.Link href="/products">Perfume</Nav.Link>
                        <Nav.Link href="/products">Bag</Nav.Link>
                        <Nav.Link href="/products">Clothes</Nav.Link>
                        <Nav.Link href="/products">Coronet</Nav.Link>
                    </Grid>
                </Grid>
            </Grid>
            <Grid href="" style={{ marginLeft: "20px", marginRight: "20px" }}>
                <Grid className="dropdown">
                    <h6 className="drop-btn">Shop</h6>
                    <Grid className="dropdown-content">
                        <Nav.Link href="/cart">Cart</Nav.Link>
                        <Nav.Link href="/checkOut">Checkout</Nav.Link>
                        {/* <Nav.Link href="">My Account</Nav.Link> */}
                        <Nav.Link href="">Shop Detail</Nav.Link>
                    </Grid>
                </Grid>
            </Grid>
            <Grid href="" style={{ marginLeft: "20px", marginRight: "20px" }}>
                <Grid className="dropdown">
                    <h6 className="drop-btn">Order Management</h6>
                    { user?.admin ? 
                    <Grid className="dropdown-content">
                        <Nav.Link href="/administration">administration</Nav.Link>
                        {/* <Nav.Link href="">Product Management</Nav.Link>
                        <Nav.Link href="">General Management</Nav.Link> */}
                    </Grid> :
                     <Grid className="dropdown-content">
                        {/* <Nav.Link href="">My order</Nav.Link> */}
                        <Nav.Link href="/myAccount">My Account</Nav.Link>
                    </Grid>
                    }
                    
                </Grid>
            </Grid>
            <Nav.Link href="" style={{ marginLeft: "20px", marginRight: "20px", marginTop: "2px" }}> <h6>Contact Us</h6></Nav.Link>
        </Col>
    )
}
export default HeaderLink