import { Grid } from "@mui/material"
import { Col, Navbar, NavItem } from "react-bootstrap"
import HeaderIcon from "./headerIcon"
import HeaderLink from "./headerLink"
import HeaderLogo from "./headrLogo"

function Header1 (){
    return(
        <Grid style={{ backgroundColor:"#fff"}} >
            <Navbar>
                <Col lg={2} md={2} sm={2} xs={4}>
                    <NavItem>
                        <HeaderLogo></HeaderLogo>
                    </NavItem>
                </Col>
                <Col lg={8} md={8}  sm={8}  xs={4} >
                    <NavItem >
                        <HeaderLink></HeaderLink>
                    </NavItem>
                </Col>
                <Col lg={2} md={2} sm={2} xs={4}>
                    <NavItem >
                        <HeaderIcon></HeaderIcon>
                    </NavItem>
                </Col>
            </Navbar>
        </Grid>
    )
}

export default Header1