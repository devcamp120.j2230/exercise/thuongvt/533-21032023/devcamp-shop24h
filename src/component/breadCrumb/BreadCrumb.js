
import * as React from 'react';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';



export default function BreadCrumb(props) {
    const links = JSON.parse(props.link);
    return (
        <Stack className='breadCrumb-cover'>
            <Breadcrumbs className='breadCrumb' separator="/" aria-label="breadcrumb" style={{marginTop:"10px", marginBottom:"10px", marginLeft:"40px"}}>
                {links.map((e,i)=>{
                    return <Link  underline="hover"  color="inherit" href={e.url} key={i}>{e.name}</Link>
                })}
            </Breadcrumbs>
        </Stack>
        
    );
}
