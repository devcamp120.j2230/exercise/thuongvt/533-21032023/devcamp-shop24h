import { Grid, Typography } from "@mui/material";
import Rating from '@mui/material/Rating';
import { useDispatch } from "react-redux";
import { getItemProduct } from "../../actions/cart.action";
import { useNavigate } from "react-router-dom";
import { useState } from "react";


function ShowData(props) {

    const data = props.data
    const dispatch = useDispatch()
    const navigator= useNavigate()
    const [count, setCount] = useState(1)// count sử dụng cho phần productType

    const onClickBtnAddCart = () => {
        // Tạo mảng chứa phần tử cần lưu trữ
        var cartArray = []
        //lấy danh sách sản phẩm từ localstorage
        cartArray = localStorage.getItem("Product") ? JSON.parse(localStorage.getItem("Product")) : [];
        // console.log(cartArray.length)
        //kiểm tra sản phẩm đã được thêm vào cart chưa?
        //nếu chưa thì thêm vào cart
        var check = -1;

        if (cartArray.length !== 0) {
            cartArray.find((item, key) => {
                if (item._id === data._id) {
                    check = key
                }
            });
        }

        if (check < 0) {
            cartArray.push({ ...data,count })
        }
        //thêm thay đổi vào localstorage
        localStorage.setItem("Product", JSON.stringify(cartArray));
        // gọi hàm để chuyển giá trị của mảng lưu lên giỏ hàng
        dispatch(getItemProduct(cartArray.length))
    }

    // Hàm khi ấn nút come back
    const onClickBtnComeBack =()=>{
        navigator(`/products`)
    }

    return (
        <Grid display="flex" mt={8} mb={8} flexWrap="wrap">
            <Grid xs={6} md={8} sm={12}>
                <img src={props.data.imageUrl} alt="photo-product-detail" className="product-detail-show-img"></img>
            </Grid>
            <Grid xs={6} sm={12} md={8} marginLeft={4}>
                <Typography m={1}> <h4> {data.name}</h4></Typography>
                <Typography m={1}>{data.Type}</Typography>
                <Typography m={1}><Rating name="size-small" defaultValue={3} /></Typography>
                <Typography m={1}><span className="text-danger fw-bold" style={{ fontSize: "20px" }}>$ {data.buyPrice}</span></Typography>
                <Typography m={1} maxWidth="750px" maxHeight="300px" textAlign="justify">{data.description}</Typography>
                <Typography m={1}>Amount: {data.amount}</Typography>
                <Typography m={1}>
                <button className="btn-product-cart" onClick={() => onClickBtnAddCart(data)} >ADD TO CART </button>
                <button className="btn-product-cart" onClick={onClickBtnComeBack} >COME BACK </button>
                </Typography>
            </Grid>
        </Grid>

    )
}
export default ShowData