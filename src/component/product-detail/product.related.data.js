import { useEffect } from "react";
import { Grid, Pagination } from "@mui/material"
import { useDispatch, useSelector } from "react-redux";
import { changePageAction, getProductRelated } from "../../actions/product.action";
import { Container } from "@mui/system";
import Rating from '@mui/material/Rating';
import { useNavigate } from "react-router-dom";

function ProductRelated(props) {
    // console.log(props.data.type)
    const navigator = useNavigate()
    const ProductRelate = props.data.type
    const dispatch = useDispatch()

    const { products, currentPage, countPage, limitProduct } = useSelector((reduxData) => {
        return reduxData.productReducer
    })

    useEffect(() => {
        dispatch(getProductRelated(ProductRelate, currentPage, limitProduct))
    }, [currentPage,ProductRelate])


    const handleChangePage = (e, v) => {
        dispatch(changePageAction(v))
    }

    const btnView = (element) => {
        // console.log(element)
        if (element.id !== "") {
            navigator(`/products/${element._id}`)
        }
    }

    return (
    <div className="container-fluid" style={{padding:"40px", backgroundColor:"#f5f5f5"}}>
        <div style={{marginTop:"20px"}}>
            <div className="blog">
                <h3>Ralated Products</h3>
            </div>
            <div className="blog">
                <p style={{ marginTop: "20px", opacity: "70%", marginBottom: "30px" }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet lacus enim.</p>
            </div>
        </div>
        <div item xs={12} sm={12} md={12} style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}>
            {products.slice(0,8).map((element, index) => {
                return <div className="container-product-cart" key={index}>
                    <img src={element.imageUrl}></img>
                    <div className="product-cart-icon">
                        <button className="btn-product-cart" onClick={() => { btnView(element) }} >Xem thêm</button>
                    </div>
                    <div className="product-cart-describe">
                        <div>
                            {element.name}
                        </div>
                        <div style={{ display: "flex", justifyContent: "space-between" }}>
                            <div>
                                <del style={{ opacity: "0.5" }}>$:{element.promotionPrice}</del><span style={{ color: "#d33b33" }}>   $:{element.buyPrice}</span>
                            </div>
                            <Rating name="no-value" value={null} />
                        </div>
                    </div>
                </div>

            })}
        </div>
        <Container>
            <Grid item style={{ display: "flex", justifyContent: "end", marginBottom: "10px" }} >
                <Pagination count={countPage} defaultPage={currentPage} onChange={handleChangePage}></Pagination>
            </Grid>
        </Container>
    </div>)
}
export default ProductRelated