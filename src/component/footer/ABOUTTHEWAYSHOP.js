import { Grid} from "@mui/material"
import { Col, Row } from "react-bootstrap"
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import WifiIcon from '@mui/icons-material/Wifi';
import ColorLensIcon from '@mui/icons-material/ColorLens';
import WifiCalling3Icon from '@mui/icons-material/WifiCalling3';

function ABOUTTHEWAYSHOP() {
    return (
        <Grid>
            <Row>
                <Col className="container" style={{marginLeft:"100px"}}>
                <h4  style={{color:"white",marginTop:"70px",textAlign:"left"}}>ABOUT THEWAYSHOP</h4>
                <p style={{marginBlock:"20px", marginTop:"40px",color:"white",textAlign:"justify"}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </p>
                <Col style={{color:"white", float:"left", marginBottom:"70px",marginTop:"30px"}}>
                    <FacebookIcon style={{width:"30px", height:"30px",margin:"5px"}}></FacebookIcon>
                    <TwitterIcon style={{width:"30px", height:"30px",margin:"5px"}}></TwitterIcon>
                    <InstagramIcon style={{width:"30px", height:"30px",margin:"5px"}}></InstagramIcon>
                    <MailOutlineIcon style={{width:"30px", height:"30px",margin:"5px"}}></MailOutlineIcon>
                    <WifiIcon style={{width:"30px", height:"30px",margin:"5px"}}></WifiIcon>
                    <ColorLensIcon style={{width:"30px", height:"30px",margin:"5px"}}></ColorLensIcon>
                    <WifiCalling3Icon style={{width:"30px", height:"30px",margin:"5px"}}></WifiCalling3Icon>
                </Col>
                </Col>
            </Row>
        </Grid>
    )
}

export default ABOUTTHEWAYSHOP