import { Grid} from "@mui/material"
import { Col, Row } from "react-bootstrap"
import LocationOnIcon from '@mui/icons-material/LocationOn';
import PhoneInTalkIcon from '@mui/icons-material/PhoneInTalk';
import MailIcon from '@mui/icons-material/Mail';

function CONTACTUS (){
    return(
        <Grid>
            <Row>
                <Col className="container" style={{textAlign:"left",marginRight:"40px"}}>
                <h4  style={{color:"white",marginTop:"70px"}}>CONTACT US</h4>
                <Col style={{color:"white", float:"left", marginBottom:"70px",marginTop:"25px"}}>
                    <Row>
                    <Col sm={2}><LocationOnIcon></LocationOnIcon></Col>
                    <Col sm={10}><p>Address: Michael I. Days 3756<p>Preston Street Wichita,<p>KS 67213</p></p></p></Col>
                    </Row>
                    <Row>
                        <Col sm={2}>
                            <PhoneInTalkIcon></PhoneInTalkIcon>
                        </Col>
                        <Col sm={10}>
                            <p>Phone: 0396590326</p>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={2}>
                            <MailIcon></MailIcon>
                        </Col>
                        <Col sm={10}>
                            <p>Email: thuongcuu@gmail.com</p>
                        </Col>
                    </Row>
                </Col>
                </Col>
            </Row>
        </Grid>
    )
}

export default CONTACTUS