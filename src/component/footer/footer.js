import { Grid } from "@mui/material"
import { Col, Row } from "react-bootstrap"
import ABOUTTHEWAYSHOP from "./ABOUTTHEWAYSHOP"
import CONTACTUS from "./CONTACTUS"
import INFORMATION from "./INFORMATION"

function Footer1 (){
    return(
        <Grid style={{textAlign:"center", backgroundColor:"#202020"}}>
            <Row>
                <Col lg={4} md={12}  sm={12}><ABOUTTHEWAYSHOP></ABOUTTHEWAYSHOP></Col>
                <Col lg={4} md={12}  sm={12}><INFORMATION></INFORMATION></Col>
                <Col lg={4} md={12}  sm={12}><CONTACTUS></CONTACTUS></Col>
            </Row>
        </Grid>
    )
}

export default Footer1
