import { Grid } from "@mui/material"
import { Col, Row } from "react-bootstrap"


function INFORMATION() {
    return (
        <Grid>
            <Row>
                <Col className="container" style={{ textAlign: "left",marginLeft:"70px" }}>
                    <h4 style={{ color: "white", marginTop: "70px"}}>INFORMATION</h4>
                    <ul style={{ listStyleType: "square",color: "white",marginTop:"35px"}}>
                        <li ><p>About Us</p></li>
                        <li ><p>Customer Service</p></li>
                        <li ><p>Our Sitemap</p></li>
                        <li ><p>Terms &amp; Conditions</p></li>
                        <li ><p>Privacy Policy</p></li>
                        <li ><p>Delivery Information</p></li>
                    </ul>
                </Col>
            </Row>
        </Grid>
    )
}
export default INFORMATION