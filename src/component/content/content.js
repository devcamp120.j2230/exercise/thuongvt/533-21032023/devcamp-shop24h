import { Grid } from "@mui/material"
import Blog from "./Blog"
import ContentCarousel from "./Carousel"

import LastestProducts from "./LastestProducts"



function Content() {
  return (
    <Grid>
      <ContentCarousel></ContentCarousel>
      <LastestProducts></LastestProducts>
      <Blog></Blog>
    </Grid>
  )
}

export default Content