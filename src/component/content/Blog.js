import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import SmsIcon from '@mui/icons-material/Sms';
function Blog() {
    return (
        <div className="container-fluid" style={{padding:"60px", backgroundColor:"#f5f5f5"}}>
            <div className="blog">
                <h3>Latest blog</h3>
            </div>
            <div className="blog">
                <p style={{ marginTop: "20px", opacity:"70%", marginBottom:"50px" }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet lacus enim.</p>
            </div>
            <div className="container-fluid">
                <div className="card-blog">
                    <div className="card-blog-content">
                        <img src={"https://i.imgur.com/9CL4lqx.jpg"} class="card-img-top-blog" alt="img" />
                        <div className="container">
                            <div className="row">
                                <h5 style={{ textAlign: "center", marginTop: "10px" }}>Fusce in augue non nisi fringilla</h5>
                                <div className="col-1 sildebar">
                                    <FavoriteBorderIcon className='abc'></FavoriteBorderIcon>
                                    <RemoveRedEyeIcon className='abc'></RemoveRedEyeIcon>
                                    <SmsIcon className='abc'></SmsIcon>
                                </div>
                                <div className="col-10">
                                    <div className="content-blog">
                                        <p>Nulla ut urna egestas, porta libero id, suscipit orci. Quisque in lectus sit amet urna dignissim feugiat. Mauris molestie egestas pharetra. Ut finibus cursus nunc sed mollis. Praesent laoreet lacinia elit id lobortis.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card-blog-content col-md-6 col-lg-4 col-xl-4">
                        <img src={`https://i.imgur.com/h9l6c0s.jpg`} class="card-img-top-blog" alt="img" />
                        <div className="container">
                            <div className="row">
                                <h5 style={{ textAlign: "center", marginTop: "10px" }}>Fusce in augue non nisi fringilla</h5>
                                <div className="col-1 sildebar">
                                    <FavoriteBorderIcon className='abc'></FavoriteBorderIcon>
                                    <RemoveRedEyeIcon className='abc'></RemoveRedEyeIcon>
                                    <SmsIcon className='abc'></SmsIcon>
                                </div>
                                <div className="col-10">
                                    <div className="content-blog">
                                        <p>Nulla ut urna egestas, porta libero id, suscipit orci. Quisque in lectus sit amet urna dignissim feugiat. Mauris molestie egestas pharetra. Ut finibus cursus nunc sed mollis. Praesent laoreet lacinia elit id lobortis.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card-blog-content col-md-6 col-lg-4 col-xl-4">
                        <img src={"https://i.imgur.com/9Tzznrg.jpg"} class="card-img-top-blog" alt="img" />
                        <div className="container">
                            <div className="row">
                                <h5 style={{ textAlign: "center", marginTop: "10px" }}>Fusce in augue non nisi fringilla</h5>
                                <div className="col-1 sildebar">
                                    <FavoriteBorderIcon className='abc'></FavoriteBorderIcon>
                                    <RemoveRedEyeIcon className='abc'></RemoveRedEyeIcon>
                                    <SmsIcon className='abc'></SmsIcon>
                                </div>
                                <div className="col-10">
                                    <div className="content-blog">
                                        <p>Nulla ut urna egestas, porta libero id, suscipit orci. Quisque in lectus sit amet urna dignissim feugiat. Mauris molestie egestas pharetra. Ut finibus cursus nunc sed mollis. Praesent laoreet lacinia elit id lobortis.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Blog