import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


function ContentCarousel() {

    const settings = {
        dots: true,
        infinite: true,
        speed: 200,
        slidesToShow: 1,
        slidesToScroll: 1,
        
    }
    return (
        <div>
            <Slider {...settings}>
                <div>
                    <div className="cover-carosel" style={{backgroundImage:`url(https://i.imgur.com/w5y5G8b.jpg)`, height:"550px", filter:"blur(80%)"}} >
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="row">
                                        <div className="col-sm-6 carousel-text " style={{color:"#010101", marginTop:"130px"}}>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold">Áo khoác</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold abc" style={{ fontSize: "30px" }}>Áo khoác khuy móc</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p style={{ lineHeight: "1.5" }}>Lấy cảm hứng thiết kế, sáng tạo độc đáo từ chiếc áo khoác mang kiểu dáng của người lính hải quân Anh, Áo Toggle Coat là một item khá nổi tiếng và được nhiều giới trẻ lựa chọn trong mùa đông năm nay với những chiếc áo măng tô và áo khoác demin mang đến cho bạn phong cách mới mẻ, trẻ trung và vô cùng ấn tượng.
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <button className="btn mt-4 shop-now" style={{ borderRadius: "20px", backgroundColor:"#d33b33", color:"white" }}>SHOP NEW</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            {/* <img className="carousel-image" alt="carousel-1" style={{width:"400px"}}/> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div  className="cover-carosel" style={{backgroundImage:`url(https://i.imgur.com/iu5H488.jpg)`, height:"550px", filter:"blur(80%)"}}>
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="row">
                                        <div className="col-sm-6 carousel-text" style={{color:"white",marginTop:"130px"}}>
                                            <div className="row" >
                                                <div className="col-sm-12">
                                                    <p className="fw-bold">Lelabo</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold " style={{ fontSize: "50px" }}>Santal 33 Eau de Parfum</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p style={{ lineHeight: "1.5", opacity: "0.8" }}>Santal 33 Eau de Parfum: Our Santal 33 fragrance is a cult favorite among men. This scent features a unique blend of sandalwood, leather, and spices for a warm and sophisticated scent. It's perfect for a night out or a special occasion.
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <button className="btn mt-4 shop-now" style={{ borderRadius: "20px", backgroundColor:"#d33b33", color:"white" }}>SHOP NEW</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            {/* <img className="carousel-image" src={imgCarousel2} alt="carousel-2" style={{width:"400px"}}/> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div  className="cover-carosel" style={{backgroundImage:`url(https://i.imgur.com/QE0KID4.jpg)`, height:"550px", filter:"blur(80%)"}}>
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="row">
                                        <div className="col-sm-6 carousel-text" style={{color:"white",marginTop:"130px"}} >
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold">Lelabo</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold " style={{ fontSize: "50px" }}>The Noir 29 Eau de Parfum</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p style={{ lineHeight: "1.5", opacity: "0.8" }}>Add a touch of mystery with our Noir 29 fragrance. This scent features a blend of black pepper, fig, and vanilla notes for a dark and seductive scent. It's perfect for a date night or a night out on the town.
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <button className="btn mt-4 shop-now" style={{ borderRadius: "20px", backgroundColor:"#d33b33", color:"white" }}>SHOP NEW</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            {/* <img className="carousel-image" src={imgCarousel3} alt="carousel-1" style={{width:"400px"}}/> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div>
                    <div  className="cover-carosel" style={{backgroundImage:`url(https://i.imgur.com/DZqphx7.jpg)`, height:"550px", filter:"blur(80%)"}}>
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="row">
                                        <div className="col-sm-6 carousel-text" style={{color:"white",marginTop:"130px"}} >
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold">JBL Quantum 100</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold " style={{ fontSize: "50px" }}>Ipsum dolor</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p style={{ lineHeight: "1.5", opacity: "0.8" }}>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                                                        voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non
                                                        provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <button className="btn mt-4 shop-now" style={{ borderRadius: "20px", backgroundColor:"#d33b33", color:"white" }}>SHOP NEW</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            {/* <img className="carousel-image" src={imgCarousel3} alt="carousel-1" style={{width:"400px"}}/> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </Slider>
        </div>
    )
}

export default ContentCarousel