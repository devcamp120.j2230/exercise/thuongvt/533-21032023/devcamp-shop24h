import { ButtonBase, Grid } from "@mui/material"
import { Container } from "@mui/system"
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllProduct } from "../../actions/product.action";

function LastestProducts() {

    const dispatch = useDispatch()

    const { products } = useSelector((reduxData) => {
        return reduxData.productReducer
    })

    useEffect(() => {
        dispatch(getAllProduct())
    }, [])

    return (
        <>
            <Grid item xs={3} mt={5} >
                <Grid style={{ textAlign: "center", marginTop: "100px", marginBottom: "80px" }}>
                    <div>
                        <h3>PRODUCTS.</h3>
                    </div>
                    <div style={{ textAlign: "center", marginTop: "20px", opacity: "70%" }}>
                        <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
                    </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={3} style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}>
                    {products.slice(0, 6).map((element, index) => {
                        return (
                            <div className="card" key={index}>
                                <img src={element.imageUrl} class="card-img-top" alt="img" />
                                <div className="card-body">
                                    <h5 className="card-title">{element.name}</h5>
                                </div>
                            </div>
                        )
                    })}
                </Grid>
            </Grid>
            <Grid textAlign="center" className="view-all">
                <ButtonBase href="/products" style={{ margin: "20px", color: "white" }}><h5>View all product...</h5></ButtonBase>
            </Grid>
        </>
    )
}
export default LastestProducts