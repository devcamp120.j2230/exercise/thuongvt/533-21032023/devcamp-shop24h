import { Alert, Grid, Modal, Snackbar } from "@mui/material";
import Box from '@mui/material/Box';
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Button, Col, Container, Row } from "reactstrap";
import { ModalCheckCloseAction } from "../../actions/modal.action";




const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: "white",
    boxShadow: 20,
    p: 3,
};


function ModalCheckOut() {

    //Khai báo một số state sử dụng nội bộ funcion
    const dispatch = useDispatch()
    const navigator = useNavigate()
    const [Close, setClose] = useState()

    const { openModalCheck } = useSelector((reduxData) => {
        return reduxData.ModalLogInReducer
    })


    const btnContinue = () => {
        navigator(`/products`)
        setClose(dispatch(ModalCheckCloseAction()))
    }


    return (
        <Grid>
            <Modal
                open={openModalCheck}
                onClose={Close}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Container >
                        <Col style={{ textAlign: "center", color: "black" }}><h3>Order Success</h3></Col>
                        <Row style={{ display: "flex", justifyContent: "center" }}>
                            <button type="submit" className="btn-Register" style={{ marginTop: "20px", width: "120px", marginRight: "20px" }}>Order Check</button>
                            <button type="submit" className="btn-Register" style={{ marginTop: "20px", width: "120px", marginRight: "20px" }} onClick={btnContinue} >Keep Shopping</button>
                        </Row>
                    </Container>
                </Box>
            </Modal>
            {/* <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert sx={{ width: '100%' }}>
                    {alertForm}
                </Alert>
            </Snackbar> */}
        </Grid>
    )
}

export default ModalCheckOut
