import { Grid, Modal, Typography } from "@mui/material";
import Box from '@mui/material/Box';
import { useDispatch, useSelector } from "react-redux";
import { Button, Col, Container, Row } from "reactstrap";
import { ModalLogInCloseAction } from "../../actions/modal.action";
import { logInWithEmail, login } from "../../actions/login.action";
import { Close } from "@mui/icons-material";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";



const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: "white",
    boxShadow: 24,
    p: 3,
    textAlign: "center"
};


function ModalLogIn() {

    const [Close, setClose] = useState()
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const dispatch = useDispatch()
    const navigator = useNavigate()

    const { openModalLogin } = useSelector((reduxData) => {
        return reduxData.ModalLogInReducer
    })
    
    const { user } = useSelector((reduxData) => {
        return reduxData.loginReducer
    })
    //login ggg
    const btnLogInGoogle = () => {
        dispatch(login())
    }
    //login Email
    const btnLoginWittEmail= ()=>{
        const check = validateLogin(email,password)
        if(check){
            const data = {
                email: email,
                password:password
            }
            dispatch(logInWithEmail(data))
        }
    }

    const btnCancel = () => {
        setClose(dispatch(ModalLogInCloseAction()))
    }
    const btnRegisterNewAcc = () => {
        navigator(`/checkOut`)
        setClose(dispatch(ModalLogInCloseAction()))
    }

    //hàm xử lý kiểm tra dữ liệu login
    const validateLogin = (email, password)=>{
        if (email === "") {
            alert("email không được để trống");
            return false
        }
        if (password === "") {
            alert("password không được để trống");
            return false
        }
        return true
    }

    useEffect(() => {
        setClose(dispatch(ModalLogInCloseAction()))
    }, [user])


    return (
        <Grid>
            <Modal
                open={openModalLogin}
                onClose={Close}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Col style={{ textAlign: "center", color: "black" }}><h3>Log In Account</h3></Col>
                    <Container>
                        <div className="row">
                            <div className="title-left">
                                <button type="submit" className="btn-Register" style={{ width: "300px", height: "40px", margin: "5px" }} onClick={btnLogInGoogle}>LogIn with Google</button>
                            </div>
                        </div>
                        <div className="row" style={{ marginTop: "10px" }}>
                            <div className="form-group" style={{ textAlign: "start" }}>
                                <label for="" className="mb-0">Email Address</label>
                                <input type="email" className="form-control" placeholder="Enter Email" onChange={(e) => setEmail(e.target.value)}  ></input>
                            </div>
                            <div className="form-group" style={{ textAlign: "start" }}>
                                <label for="InputEmail" className="mb-0">Password</label>
                                <input type="password" className="form-control" placeholder="Password" onChange={(e) => setPassword(e.target.value)}></input>
                            </div>
                            <div className="title-left">
                                <button type="submit" className="btn-Register" style={{ width: "300px", height: "40px", margin: "5px" }} onClick={btnLoginWittEmail}>LogIn with Email</button>
                            </div>
                        </div>
                        <div className="row" style={{ marginTop: "5px" }}>
                            <div className="form-group col-md-6">
                                <p style={{ textAlign: "start" }}>Register new account</p>
                                <button type="submit" className="btn-Register" style={{ minHeight: "50px", minWidth: "100px" }} onClick={btnRegisterNewAcc}>Register</button>
                            </div>
                            <div className="form-group col-md-6">
                                <p style={{ textAlign: "start" }}>Cancel Register</p>
                                <button type="submit" className="btn-Register" style={{ minHeight: "50px", minWidth: "100px" }} onClick={btnCancel}>Cancel</button>
                            </div>
                        </div>
                    </Container>
                </Box>
            </Modal>
        </Grid>
    )
}

export default ModalLogIn
