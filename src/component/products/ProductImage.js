

function ProductImage(props) {
    // console.log(props.data)
    return (
        <div className="product-header">
            <img srcSet={"https://i.imgur.com/owgQ04W.jpg"}></img>
            <div className="product-img-header" >
                <p style={{ fontSize: "25px" }}>The way shop</p>
                <h2>{props.data.Name}</h2>
            </div>
        </div>
    )
}
export default ProductImage