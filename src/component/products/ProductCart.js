import { Grid, Pagination } from "@mui/material"
import { Container } from "@mui/system";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { changePageAction, getAllProduct } from "../../actions/product.action";
import Rating from '@mui/material/Rating';


function ProductCart() {

    const navigator = useNavigate()
    const dispatch = useDispatch()


    const { products, currentPage, countPage, limitProduct } = useSelector((reduxData) => {
        return reduxData.productReducer
    })

    console.log(products)
    useEffect(() => {
        dispatch(getAllProduct(currentPage, limitProduct))
    }, [currentPage])


    const handleChangePage = (e, v) => {
        dispatch(changePageAction(v))
    }

    const btnChitiet = (element) => {
        // console.log(element)
        if (element.id !== "") {
            navigator(`/products/${element._id}`)
        }
    }

    return (<>
        <Grid>
            <Grid item xs={12} sm={12} md={12} margin={2} style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}>
                {products.map((element, index) => {
                    return <div className="container-product-cart" key={index}>
                        <img src={element.imageUrl}></img>
                        <div className="product-cart-icon">
                            <button className="btn-product-cart" onClick={() => { btnChitiet(element) }} >VIEW </button>
                            <button className="btn-product-cart" >ADD TO CART </button>
                        </div>
                        <div className="product-cart-describe">
                            <div>
                                {element.name}
                            </div>
                            <div style={{ display: "flex", justifyContent: "space-between" }}>
                                <div>
                                    <del style={{ opacity: "0.5" }}>$:{element.promotionPrice}</del><span style={{ color: "#d33b33" }}>   $:{element.buyPrice}</span>
                                </div>
                                <Rating name="no-value" value={null} />
                            </div>
                        </div>
                    </div>
                })}

            </Grid>
        </Grid>

        <Container>
            <Grid item style={{ display: "flex", justifyContent: "end", marginBottom: "10px" }} >
                <Pagination count={countPage} defaultPage={currentPage} onChange={handleChangePage}></Pagination>
            </Grid>
        </Container>
    </>
    )
}

export default ProductCart