import Checkbox from '@mui/material/Checkbox';
import { Button, FormControlLabel, FormGroup, Grid, TextField, Typography } from "@mui/material"
import Rating from '@mui/material/Rating';
import Stack from '@mui/material/Stack';
import { useState } from "react";


function ProductFilter() {
    // xử lý sự kiện filter pice
    const [checkedOnSale, setCheckedOnSale] = useState(false);
    const [checkedInStock, setCheckedInStock] = useState(false);
    const [checkedFeature, setCheckedFeature] = useState(false)

    const handleChangeOnSale = (event) => {
        setCheckedOnSale(event.target.checked);
        setCheckedInStock(false);
        setCheckedFeature(false);
    };
    const handleChangeInStock = (event) => {
        setCheckedOnSale(false);
        setCheckedInStock(event.target.checked);
        setCheckedFeature(false);
    };

    const handleChangeFeature = (event) => {
        setCheckedOnSale(false);
        setCheckedInStock(false);
        setCheckedFeature(event.target.checked);
    };
    //Xử lý sư kiện filter brand
    const [checkedJBL, setCheckJBL] = useState(false)
    const [checkedBeat, setCheckBeat] = useState(false)
    const [checkedLogitech, setCheckLogitech] = useState(false)
    const [checkedSamSung, setCheckSamSung] = useState(false)
    const [checkedSony, setCheckSony] = useState(false)

    const handleChangeJBL = (event) => {
        setCheckBeat(false)
        setCheckJBL(event.target.checked)
        setCheckLogitech(false)
        setCheckSamSung(false)
        setCheckSony(false)
    }
    const handleChangeBeat = (event) => {
        setCheckBeat(event.target.checked)
        setCheckJBL(false)
        setCheckLogitech(false)
        setCheckSamSung(false)
        setCheckSony(false)
    }
    const handleChangeLogitech = (event) => {
        setCheckBeat(false)
        setCheckJBL(false)
        setCheckLogitech(event.target.checked)
        setCheckSamSung(false)
        setCheckSony(false)
    }
    const handleChangeSamSung = (event) => {
        setCheckBeat(false)
        setCheckJBL(false)
        setCheckLogitech(false)
        setCheckSamSung(event.target.checked)
        setCheckSony(false)
    }
    const handleChangeSony = (event) => {
        setCheckBeat(false)
        setCheckJBL(false)
        setCheckLogitech(false)
        setCheckSamSung(false)
        setCheckSony(event.target.checked)
    }
    //Xử lý sư kiện filter color
    const [checkedRed, setCheckRed] = useState(false);
    const [checkedBLue, setCheckBLue] = useState(false);
    const [checkedWhite, setCheckWhite] = useState(false);
    const [checkedPink, setCheckPink] = useState(false);
    const [checkedYellow, setCheckYellow] = useState(false);

    const handleChangeRed = (event) => {
        setCheckRed(event.target.checked);
        setCheckPink(false)
        setCheckWhite(false)
        setCheckYellow(false)
        setCheckBLue(false)
    }
    const handleChangeBlue = (event) => {
        setCheckRed(false);
        setCheckPink(false)
        setCheckWhite(false)
        setCheckYellow(false)
        setCheckBLue(event.target.checked)
    }
    const handleChangePink = (event) => {
        setCheckRed(false);
        setCheckPink(event.target.checked)
        setCheckWhite(false)
        setCheckYellow(false)
        setCheckBLue(false)
    }
    const handleChangeWhite = (event) => {
        setCheckRed(false);
        setCheckPink(false)
        setCheckWhite(event.target.checked)
        setCheckYellow(false)
        setCheckBLue(false)
    }
    const handleChangeYellow = (event) => {
        setCheckRed(false);
        setCheckPink(false)
        setCheckWhite(false)
        setCheckYellow(event.target.checked)
        setCheckBLue(false)
    }

    //Xử lý sư kiện filter rating

    return (
        <Grid margin={2}>
            <Grid item xs={6} sm={12} md={12}>
                <Typography style={{ fontSize: '15px', fontWeight: "bold" }}>Categorles</Typography>
                <Typography style={{ fontSize: "15px" }} className="mt-3">Wireless</Typography>
                <Typography style={{ fontSize: "15px" }} className="mt-3">In-ear headphone</Typography>
                <Typography style={{ fontSize: "15px" }} className="mt-3">Over-ear headphone</Typography>
                <Typography style={{ fontSize: "15px" }} className="mt-3">Sport headphone</Typography>
            </Grid>
            <Grid item xs={6} sm={12} md={12} mt={4}>
                <Typography style={{ fontSize: '15px', fontWeight: "bold" }}>Pice</Typography>
                <FormGroup>
                    <Grid item display="flex" justifyContent="space-between">
                        <TextField id="standard-basic" label="Pice Min" variant="standard" style={{ width: "70px", maxHeight: "50px" }}></TextField>
                        <TextField id="standard-basic" label="Pice Max" variant="standard" style={{ width: "70px", maxHeight: "50px" }}></TextField>
                    </Grid>
                    <Grid mt={2}>
                        {/* <Button variant="contained">search</Button> */}
                    </Grid>
                    <FormControlLabel control={<Checkbox
                        checked={checkedOnSale}
                        onChange={handleChangeOnSale}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="On Sale" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedInStock}
                        onChange={handleChangeInStock}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="In Stock" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedFeature}
                        onChange={handleChangeFeature}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Featured" />
                </FormGroup>
                {/* <Button variant="contained">search</Button> */}
            </Grid>
            <Grid item xs={6} sm={12} md={12} mt={4}>
                <Typography style={{ fontSize: '15px', fontWeight: "bold" }}>Brand</Typography>
                <FormGroup>
                    <FormControlLabel control={<Checkbox
                        checked={checkedJBL}
                        onChange={handleChangeJBL}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="JBL" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedSamSung}
                        onChange={handleChangeSamSung}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Sam Sung" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedBeat}
                        onChange={handleChangeBeat}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Baet" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedLogitech}
                        onChange={handleChangeLogitech}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Logitech" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedSony}
                        onChange={handleChangeSony}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Sony" />
                </FormGroup>
                {/* <Button variant="contained">search</Button> */}
            </Grid>
            <Grid item xs={6} sm={12} md={12} mt={4}>
                <Typography style={{ fontSize: '15px', fontWeight: "bold" }}>Colors</Typography>
                <FormGroup>
                    <FormControlLabel control={<Checkbox
                        checked={checkedRed}
                        onChange={handleChangeRed}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Red" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedBLue}
                        onChange={handleChangeBlue}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="BLue" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedWhite}
                        onChange={handleChangeWhite}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="White" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedPink}
                        onChange={handleChangePink}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Pink" />
                    <FormControlLabel control={<Checkbox
                        checked={checkedYellow}
                        onChange={handleChangeYellow}
                        inputProps={{ "aria-label": "controlled" }}
                    />} label="Yellow" />
                </FormGroup>
                <Button variant="contained">search</Button>
            </Grid>
            <Grid item xs={6} sm={12} md={12} mt={4}>
                <Typography style={{ fontSize: '15px', fontWeight: "bold" }}>Rating</Typography>
                <Typography style={{ fontSize: "15px" }} className="mt-2">Đánh giá về chúng tôi</Typography>
                <Stack spacing={1} mt={2}>
                    <Rating name="size-medium" defaultValue={3} />
                </Stack>
            </Grid>
        </Grid>
    )
}
export default ProductFilter