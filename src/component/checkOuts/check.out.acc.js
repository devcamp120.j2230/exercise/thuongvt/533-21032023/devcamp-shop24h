
import { useEffect, useState } from "react"
import axios from "axios"
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { logInWithEmail } from "../../actions/login.action";



function Account() {
    const fetchAPI = async (url, requestOptions) => {
        let res = await fetch(url, requestOptions);
        let data = await res.json()
        return data
    }

    const { user } = useSelector((reduxLogIn) => {
        return reduxLogIn.loginReducer
    })

    const dispatch = useDispatch();
    const navigator = useNavigate()
    // nơi xử lý sự kiện lấy địa chỉ tỉnh thành
    const [city, setCity] = useState([])
    const [idCity, setIdCity] = useState("")
    const [district, setDistrict] = useState([])
    const [idDistrist, setIdDistrict] = useState("")
    const [ward, setWard] = useState([])
    const [IdWard, setIdward] = useState("")
    const [street, SetStreet] = useState("");

    // Nơi khai báo các state Create New Account
    const [firstName, setFirstName] = useState("");
    const [lastName, SetLastName] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [linkImg, setLinkImg] = useState("");

    // xử lý sự kiện đóng mở Account Login 
    const [state, setState] = useState("none")
    const [state1, setState1] = useState("none")




    // Nơi xử lý các sự kiện chọn địa chỉ
    const callAPI = (api) => {
        return axios.get(api)
            .then((response) => {
                // console.log(response.data)
                setCity(response.data)
            });
    }
    const callApiDistrict = (api) => {
        return axios.get(api)
            .then((response) => {
                // console.log(response.data.districts)
                setDistrict(response.data.districts)
            });
    }

    const callApiWard = (api) => {
        return axios.get(api)
            .then((response) => {
                // console.log(response.data.wards, "ward");
                setWard(response.data.wards)
            });
    }

    //Nơi xử lý sự kiện bấm nút Create New Account
    const btnClickRegister = () => {
        // Kiểm tra dữ liệu đàu vào
        const check = validateRegister(firstName, lastName, phone, linkImg, idCity, idDistrist, IdWard, street, email, password)
        if (check) {
            const data = {
                firstName: firstName,
                lastName: lastName,
                phone: phone,
                email: email,
                street: street,
                city: idCity,
                province: idDistrist,
                wards: IdWard,
                image: linkImg,
                password: password
            }

            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            // gọi API them người dùng mới
            fetchAPI("http://localhost:8000/register", { method: 'POST', headers: myHeaders, body: JSON.stringify(data), redirect: 'follow' })
                .then((res) => {
                    console.log(res)
                    if (res.message === "Tạo dữ liệu customer thành công") {
                        navigator(`/cart`)
                    }
                    else (
                        alert(res.message)
                    )


                })
                .catch((err) => {
                    console.error(err)
                    alert(err.message)
                });
        }
    }

    // Xử lý sự kiện login
    const btnLogIn = () => {
        const check = validateLogin(email, password)
        if (check) {
            const data = {
                email: email,
                password: password
            }
            dispatch(logInWithEmail(data))
            navigator(`/cart`)
        }
    }

    // Hàm kiểm tra dữ liệu from Register
    const validateRegister = (firstName, lastName, phone, linkImg, idCity, idDistrist, IdWard, street, email, password) => {
        if (firstName === "") {
            alert("first name không được để trống");
            return false;
        }
        if (lastName === "") {
            alert("last name không được để trống");
            return false;
        }
        if (phone === "") {
            alert("phone không được để trống");
            return false
        }
        if (linkImg === "") {
            alert("link img không được để trống");
            return false
        }
        if (idCity === "") {
            alert("city không được để trống");
            return false
        }
        if (idDistrist === "") {
            alert("district không được để trống");
            return false
        }
        if (IdWard === "") {
            alert("ward không được để trống");
            return false
        }
        if (street === "") {
            alert("street không được để trống");
            return false
        }
        if (email === "") {
            alert("email không được để trống");
            return false
        }
        if (password === "") {
            alert("password không được để trống");
            return false
        }
        return true
    }

    //hàm xử lý kiểm tra dữ liệu login
    const validateLogin = (email, password) => {
        if (email === "") {
            alert("email không được để trống");
            return false
        }
        if (password === "") {
            alert("password không được để trống");
            return false
        }
        return true
    }

    useEffect(() => {
        callAPI('https://provinces.open-api.vn/api/?depth=1');

        if (idCity !== "") {
            callApiDistrict(`https://provinces.open-api.vn/api/p/${idCity}?depth=2`)
        }

        if (idDistrist !== "") {
            callApiWard(`https://provinces.open-api.vn/api/d/${idDistrist}?depth=2`)
        }

    }, [idCity, idDistrist])

    return (
        <>
            {user ? <></> : <>
                <div className="row">
                    <div className="col-sm-6 col-lg-6 mb-3">
                        <div className="title-left">
                            <h5>Account Login</h5>
                        </div>

                        <a onClick={() => { setState("block") }} onDoubleClick={() => { setState("none") }}><h6>Click here to Login</h6></a>

                        <div className="row" style={{ display: state }}>
                            <div className="form-group col-md-6">
                                <label for="" className="mb-0">Email Address</label>
                                <input type="email" className="form-control" placeholder="Enter Email" onChange={(e) => setEmail(e.target.value)}></input>
                            </div>
                            <div className="form-group col-md-6">
                                <label for="InputEmail" className="mb-0">Password</label>
                                <input type="password" className="form-control" placeholder="Password" onChange={(e) => setPassword(e.target.value)}></input>
                            </div>
                            <div>
                                <button type="submit" className="btn-Register" style={{ marginTop: "20px" }} onClick={btnLogIn} >Login</button>
                            </div>

                        </div>


                    </div>
                    <div className="col-sm-6 col-lg-6 mb-3">
                        <div className="title-left">
                            <h5>Create New Account</h5>
                        </div>
                        <a onClick={() => { setState1("block") }} onDoubleClick={() => { setState1("none") }}><h6>Click here to Register</h6></a>
                        <div style={{ display: state1 }}>
                            <div className="row" >
                                <div className="form-group col-md-6">
                                    <label for="" class="mb-0">First Name</label>
                                    <input type="text" className="form-control" placeholder="Enter First Name" onChange={(e) => setFirstName(e.target.value)} ></input>
                                </div>
                                <div className="form-group col-md-6">
                                    <label for="" className="mb-0">Last Name</label>
                                    <input type="text" className="form-control" placeholder="Enter Last Name" onChange={(e) => SetLastName(e.target.value)}></input>
                                </div>
                            </div>
                            <div className="row" style={{ marginTop: "20px" }}>
                                <div className="form-group col-md-6">
                                    <label for="" class="mb-0">Phone</label>
                                    <input type="email" className="form-control" placeholder="Enter Phone" onChange={(e) => setPhone(e.target.value)} ></input>
                                </div>
                                <div className="form-group col-md-6">
                                    <label for="" className="mb-0">Link image</label>
                                    <input type="text" className="form-control" placeholder="Enter Link" onChange={(e) => setLinkImg(e.target.value)}></input>
                                </div>
                            </div>
                            <div className="row" style={{ marginTop: "20px" }}>
                                <div className="form-group col-md-6">
                                    <label for="" class="mb-0">Town / City(*)</label>
                                    <select name="" placeholder="Choose City" class="form-control" onChange={(e) => { setIdCity(e.target.value) }}>
                                        <option value="">Choose City</option>
                                        {city.map((e, i) => {
                                            return <option key={i} value={e.code}>{e.name}</option>
                                        })}
                                    </select>
                                </div>
                                <div className="form-group col-md-6">
                                    <label for="" class="mb-0">District/province(*)</label>
                                    <select name="" placeholder="Choose District" class="form-control" onChange={(e) => { setIdDistrict(e.target.value) }}>
                                        <option value="">Choose District</option>
                                        {district.map((e, i) => {
                                            return <option key={i} value={e.code} >{e.name}</option>
                                        })}
                                    </select>
                                </div>
                            </div>
                            <div className="row" style={{ marginTop: "20px" }}>
                                <div className="form-group col-md-6">
                                    <label for="" class="mb-0">Sub-district/ wards(*)</label>
                                    <select name="" placeholder="Choose Commune" class="form-control" onChange={(e) => { setIdward(e.target.value) }}>
                                        <option value="">Choose wards</option>
                                        {ward.map((e, i) => {
                                            return <option key={i} value={e.code}>{e.name}</option>
                                        })}
                                    </select>
                                </div>
                                <div className="form-group col-md-6">
                                    <label for="" className="mb-0">Street/Hamlet(*)</label>
                                    <input type="text" className="form-control" placeholder="Street/Hamlet" onChange={(e) => SetStreet(e.target.value)}></input>
                                </div>
                            </div>
                            <div className="row" style={{ marginTop: "20px" }}>
                                <div className="form-group col-md-6">
                                    <label for="" class="mb-0">Email Address</label>
                                    <input type="email" className="form-control" placeholder="Enter Email" onChange={(e) => setEmail(e.target.value)} ></input>
                                </div>
                                <div className="form-group col-md-6">
                                    <label for="" className="mb-0">Password</label>
                                    <input type="password" className="form-control" placeholder="Password" onChange={(e) => setPassword(e.target.value)}></input>
                                </div>
                            </div>
                            <button type="submit" className="btn-Register" style={{ marginTop: "20px" }} onClick={btnClickRegister}>Register</button>

                        </div>

                    </div>
                </div>

            </>}

        </>)
}
export default Account