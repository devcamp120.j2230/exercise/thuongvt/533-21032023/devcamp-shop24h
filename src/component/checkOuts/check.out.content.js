import { Container, Grid } from "@mui/material"
import { useDispatch, useSelector } from "react-redux"
import Account from "./check.out.acc"
import ModalCheckOut from "../model/model.check.out"
import { useEffect, useState } from "react"
import { ModalCheckOpenAction } from "../../actions/modal.action"



function CheckOutContent() {


    const fetchAPI = async (url, requestOptions) => {
        let res = await fetch(url, requestOptions);
        let data = await res.json()
        return data
    }

    const dispatch = useDispatch()
    // Lấy giá trị trong store đẩy thông tin lên trang 
    const [data, setData] = useState([])
    const [comment, setComment] = useState("");
    const [shipDate, setShipDate] = useState()
    // lấy các giá trị trong total reducer để hiển thị trang
    const { subtotal, discount, total } = useSelector((reduxTotal) => {
        return reduxTotal.cartReducer
    })
    // Lấy giá trị của user
    const { user } = useSelector((reduxLogIn) => {
        return reduxLogIn.loginReducer
    })
console.log(user)
    // Hàm được gọi khi ấn nút onClickBtnPlaceOrder
    const onClickBtnPlaceOder = async () => {
        

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        // thu thập dữ liệu oder
        const check = await validate(comment, shipDate);
        if (check) {
            var dataOrderCustomer = {
                shippedDate: shipDate,
                note: comment,
                cost: total
            }
        }
        // // thu thập dữ liệu oder detail

        var dataOderDetail = await (data.map((item) => {
            return (
                {
                    quantity: item.count,
                    product: item._id
                }
            )
        }))
        console.log(dataOderDetail)

        fetchAPI(`http://localhost:8000/customers/${user._id}/orders`, { method: 'POST', headers: myHeaders, body: JSON.stringify(dataOrderCustomer), redirect: 'follow' })
            .then((res) => {
                console.log(res)
                //Tạo oder detail theo oder vừa tạo
                dataOderDetail.map((element, index) => {
                    return (
                        fetchAPI(`http://localhost:8000/orders/${res.Data._id}/orderDetails`, { method: 'POST', headers: myHeaders, body: JSON.stringify(element), redirect: 'follow' })
                            .then((res) => {
                                console.log(res)
                                if(res.status==="Create orderDetail successfully"){
                                    // xóa thông tin sản phẩm trong localStore và hiện ra modal
                                    localStorage.removeItem("Product")
                                    dispatch(ModalCheckOpenAction())
                                }
                                
                            })
                            .catch((err) => {
                                console.error(err)
                            })
                    )
                })
            })
            .catch((err) => {
                console.error(err);
            })


        // khi đã xong thì đưa ra thông báo và refresh lại trang xóa thông tin trong local store đi


    }

    // Hàm kiểm tra thông tin nhập vào 
    const validate = (comment, shipDate) => {
        if (comment === "") {
            alert("comment không được để trống")
            return false
        }
        if (!shipDate) {
            alert("ship date không được để trống")
            return false
        }
        return true
    }
    useEffect(() => {
        // lấy thông tin từ local storage
        const item = JSON.parse(localStorage.getItem("Product"));
        if (item) {
            setData(item)
        }
    }, [])



    return (
        <Container>
            <Grid mt={10} >
                {/* user? có thì hiện nơi oder còn không thì thông báo hãy đăng nhập hoặc đăng kí tài khoản */}
                <Account></Account>
                <div className="row">
                    <div className="col-sm-6 col-lg-6 mb-3">
                        <div className="title-left">
                            <h5>Billing Information</h5>
                        </div>
                        <div>
                            <table className="table" style={{ textAlign: "center", marginTop: "10px" }}>
                                {/* header */}
                                <thead style={{ backgroundColor: "#d33b33" }}>
                                    <tr style={{ color: "white" }}>
                                        <th>Images</th>
                                        <th>Product Name</th>
                                        <th>Price</th>
                                        <th>count</th>
                                    </tr>
                                </thead>
                                {/* body */}
                                <tbody>
                                    {data.map((e, i) => {
                                        return <tr key={i}>
                                            <td className="thumbnail-img">
                                                <a>
                                                    <img className="img-fluid" src={e.imageUrl} alt={`${e.name}`} style={{ width: "40px" }} />
                                                </a>
                                            </td>
                                            <td className="name-pr" >
                                                <p style={{ marginTop: "10px" }}>
                                                    {e.name}
                                                </p>
                                            </td>
                                            <td className="price-pr">
                                                <p style={{ marginTop: "10px" }}>$:{e.buyPrice} </p>
                                            </td>
                                            <td className="total-pr">
                                                <p style={{ marginTop: "10px" }} >{e.count}</p>
                                            </td>
                                        </tr>
                                    })}
                                </tbody>
                            </table>


                        </div>
                        <div className="row" style={{ marginTop: "20px" }}>
                            <div className="form-group col-md-6">
                                <label for="" class="mb-0">Comments</label>
                                <input type="text" className="form-control" placeholder="Comments" onChange={(e) => setComment(e.target.value)}></input>
                            </div>
                            <div className="form-group col-md-6">
                                <label for="" class="mb-0">Ship Date</label>
                                <input type="date" className="form-control" placeholder="" onChange={(e) => setShipDate(e.target.value)}></input>
                            </div>
                        </div>

                        <hr></hr>

                    </div>
                    <div className="col-sm-6 col-lg-6 mb-3">
                        <div className="title-left">
                            <h5>Payment Method</h5>
                        </div>
                        <div className="form-group" style={{ marginTop: "30px" }}>
                            <div className="col-md-12">
                                <div className="radio">
                                    <label><input type="radio" name="optradio" className="mr-2" /> Direct Bank Tranfer</label>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="radio">
                                    <label><input type="radio" name="optradio" className="mr-2" /> Check Payment</label>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="radio">
                                    <label><input type="radio" name="optradio" className="mr-2" /> Paypal</label>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="radio">
                                    <label><input type="radio" name="optradio" className="mr-2" /> COD</label>
                                </div>
                            </div>
                        </div>
                        <div className="title-left">
                            <h5>Cart Total</h5>
                        </div>
                        <div style={{ marginTop: "20px" }}>
                            <div className="d-flex" style={{ display: "flex", justifyContent: "space-between" }} >
                                <p>Sub Total </p>
                                <div className="font-weight-bold" ><p style={{ marginLeft: "30px" }}>$:{subtotal} </p> </div>
                            </div>
                            <div className="d-flex" style={{ display: "flex", justifyContent: "space-between" }} >
                                <p> Discount </p>
                                <div className="font-weight-bold" ><p style={{ marginLeft: "30px" }}>$:{discount} </p> </div>
                            </div>
                            <hr></hr>
                            <div className="d-flex" style={{ display: "flex", justifyContent: "space-between" }}>
                                <p>Shipping Cost</p>
                                <div className="font-weight-bold" ><p style={{ marginLeft: "30px" }}>$: {""} </p> </div>
                            </div>
                            <hr></hr>
                            <div className="d-flex" style={{ display: "flex", justifyContent: "space-between" }}>
                                <span className="text-danger fw-bold" style={{ fontSize: "20px" }}>Grand Total</span>
                                <div className="text-danger font-weight-bold" ><span style={{ marginLeft: "30px" }}>$:{total}  </span> </div>
                            </div>
                        </div>
                        <button type="submit" className="btn-Register" style={{ marginTop: "20px", maxWidth: "560px", marginRight: "20px" }} onClick={onClickBtnPlaceOder}>Place Order</button>
                        <button type="submit" className="btn-Register" style={{ marginTop: "20px", maxWidth: "560px" }}>Cancel</button>
                    </div>
                </div>
            </Grid>
            <ModalCheckOut></ModalCheckOut>
        </Container>
    )
}
export default CheckOutContent